"use strict";
export { save, load, remove };

let MongoClient = require('mongodb').MongoClient;
let dburl = "mongodb+srv://admin:tWpwGPRe8aftswu2@cluster0-dyjn6.mongodb.net/test?retryWrites=true";

let save = function(username, createdLevels, preferences, campaignProgress) { //save profile to db
    MongoClient.connect(dburl, function(err, client) {
        if(err) {
            console.log('Error occurred while connecting to MongoDB Atlas...\n',err);
            client.close();
            return {success:false, err:"Fail to connect to database"};
        }
        console.log('Connected...');
        let collection = client.db("comp4770").collection("User");
        collection.findOne({userID:username}, function(err, user) {});
        if (user === null) {
            //user doesn't exist - don't save profile
            client.close();
            return {success:false, err:"Failed to save profile"};
        }
        //~~~~~this may need editing~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        collection.update({userID:username},
            {createdLevels:createdLevels},
            {settings:preferences},
            {saveState:campaignProgress});
        client.close();
    });
    return {success:true};
}

let load = function(username) { //load profile from db
    MongoClient.connect(dburl, function(err, client) {
        if(err) {
            console.log('Error occurred while connecting to MongoDB Atlas...\n',err);
            client.close();
            return {success:false, err:"Fail to connect to database"};
        }
        console.log('Connected...');
        let collection = client.db("comp4770").collection("User");
        collection.findOne({userID:username}, function(err, user) {});
        client.close();
    });
    return {success:true, userID:user.userID, createdLevels:user.createdLevels,
        storyID:user.storyID};
}

// won't let me name function 'delete' ?
let remove = function(username) { //delete player/user from db - ever needed?
    MongoClient.connect(dburl, function(err, client) {
        if(err) {
            console.log('Error occurred while connecting to MongoDB Atlas...\n',err);
            client.close();
            return {success:false, err:"Fail to connect to database"};
        }
        console.log('Connected...');
        // V IF NEEDED V
        // to do - delete everything that user has, such as createdlevels from levels db, etc.

        let collection = client.db("comp4770").collection("User");
        collection.remove({userID:username}, {w:1}, function(err, result) {});
        client.close();
    });
    return {success:true};
}