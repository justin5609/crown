"use strict";
export default { login, signup }; // don't export encrypt or create

let MongoClient = require('mongodb').MongoClient;
let dburl = "mongodb+srv://admin:tWpwGPRe8aftswu2@cluster0-dyjn6.mongodb.net/test?retryWrites=true";

let login = function(username, password) {
    MongoClient.connect(dburl, function(err, client) {
        if(err) {
             console.log('Error occurred while connecting to MongoDB Atlas...\n',err);
             client.close();
             return {success:false, err:"Fail to connect to db"};
        }
        console.log('Connected...');
        let collection = client.db("comp4770").collection("User");

        // to do - login
        collection.findOne({userID:username}, function(err, user) {});

        if (user === null) {
            client.close();
            // this may be changed to return something more useful
            return {success:false, err:"Invalid username"};
        }
        
        //encrypt password before authentication
        password = encrypt(password);
        if (password !== user.password) {
            // this may be changed to something more useful
            client.close();
            return {success:false, err:"Invalid password"};
        }
        // to do - handle successful login
        client.close();
     });
     return {success:true ,accountNum:user.userID, levels:user.createdLevels};
}

let signup = function(username, password, email) {
    MongoClient.connect(dburl, function(err, client) {
        if(err) {
             console.log('Error occurred while connecting to MongoDB Atlas...\n',err);
             client.close();
             return {success:false, err:"Fail to connect to db"};
        }
        console.log('Connected...');
        let collection = client.db("comp4770").collection("User");
        
        // to do - signup
        // check if user exists
        collection.findOne({userID:username}, function(err, user) {});
        if (user !== null) {
            // if user can be found, it exists - username taken
            client.close();
            return {success:false, err:"Username is unavailable"};
        }

        // check if email exists - needed for password retrieval
        collection.findOne({email:email}, function(err, addr) {});
        if (addr !== null) {
            // if email can be found, it exists - email taken
            client.close();
            return {success:false, err:"Email already in use"};
        }

        // copy unencrypted password to use for login - otherwise it is encrypted twice and not work
        let upassword = password;

        // password checks (contains no ; and length, etc.) will be done client side?
        // encrypt password
        // password = encrypt(password);

        // create user, with user properties - own method for readibility
        if (!createUser(username, password, email)) {
            client.close();
            return {success:false, err:"Failed to create user"};
        }
     
        client.close();
    });
    // have new user login - should return successful login or equiv 
    return login(username, upassword);
}

let createUser = function(username, password, email) { // password given should already be encyrpted!
    MongoClient.connect(dburl, function(err, client) {
        if(err) {
             console.log('Error occurred while connecting to MongoDB Atlas...\n',err);
             client.close();
             return false;
        }
        console.log('Connected...');
        let collection = client.db("comp4770").collection("User");
        
        // properties of user to save
        // userID       password    storyID     createdLevels   achievments     settings    saveState
        // given        given       increment   empty           initial         initial     initial

        //first story id will be 1
        let story = 1;
        // get last storyID, highest in db, -1 for descending order, take first (highest)
        collection.find({}, function(err, lastUser) {}).sort({storyID:-1}).limit(1);
        // increment it for the new user - if not the first story/user
        if (storyID !== null){
            story = lastUser.storyID + 1;
        }

        // create empty array for createdLevels - user hasn't created any yet
        let levels = [];

        // ~~~~~~~~~~~~~~~~~~~to do - achievements, settings, savestate~~~~~~~~~~~~~~~~~
        // null values as placeholders
        let achievmenets;
        let settings;
        let saveState;

        // save newly created user
        collection.insert({$addToSet:{userID:username}}, {$addToSet:{password:password}},
            {$addToSet:{email:email}},
            {$addToSet:{storyID:story}}, {$addToSet:{achievements:achievements}},
            {$addToSet:{settings:settings}}, {$addToSet:{saveState:saveState}},
            {w:1}, function(err, result) {});
        
        client.close();
    });
    return true;
}

let encrypt = function(str) {
    // to do - actual encryption
    return str;
}