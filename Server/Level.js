"use strict";
export { save, load, search, remove };

let MongoClient = require('mongodb').MongoClient;
let dburl = "mongodb+srv://admin:tWpwGPRe8aftswu2@cluster0-dyjn6.mongodb.net/test?retryWrites=true";

let save = function(userID, levelID, tileArray, indexOfCheckpoint1, indxOfCheckpoint2,
        score, timeLimit, musicTrack, shop) { //save level to db
    MongoClient.connect(dburl, function(err, client) {
        if(err) {
            console.log('Error occurred while connecting to MongoDB Atlas...\n',err);
            client.close();
            return {success:false, err:"Fail to connect to database"};
        }
        console.log('Connected...');
        let collection = client.db("comp4770").collection("Level");
        // to do - actual saving
        if (levelID > 0) {
            //save as existing level being updated
            collection.update({levelID:levelID}, {tileArray:tileArray},
                {indexOfCheckpoint1:indexOfCheckpoint1},
                {indexOfCheckpoint2:indexOfCheckpoint2},
                {score:score}, {timeLimit:timeLimit},
                {musicTrack:musicTrack}, {shop:shop},
                {w:1}, function(err, result) {});
        } else {
            //save as new level - needs new level id

            // first hs being saved? - first hs will have id 1
            levelID = 1;
            // get last highscoreID, highest in db, -1 for descending order, take first (highest)
            collection.find({}, function(err, lastLevel) {}).sort({levelID:-1}).limit(1);
            // increment it for the new hs - if lastHS exists, otherwise first hs and id=1
            if (lastLevel !== null) {
                levelID = lastLevel.levelID + 1;
            }

            // update user.createdLevels with new level
            // find user
            let userCollection = client.db("comp4770").collection("User");
            userCollection.findOne({userID:userID}, function(err, user) {});
            // $push to add levelID to user's list of levelIDs
            userCollection.update({userID:userID}, {$push:{createdLevels:levelID}});

            collection.insert({levelID:levelID}, {$addToSet:{tileArray:tileArray}},
                {$addToSet:{indexOfCheckpoint1:indexOfCheckpoint1}},
                {$addToSet:{indexOfCheckpoint2:indexOfCheckpoint2}},
                {$addToSet:{score:score}}, {$addToSet:{timeLimit:timeLimit}},
                {$addToSet:{musicTrack:musicTrack}}, {$addToSet:{shop:shop}},
                {w:1}, function(err, result) {});
        }
        client.close();
    });
    return {success:true};
}

let load = function(id) { //load level from db
    MongoClient.connect(dburl, function(err, client) {
        if(err) {
            console.log('Error occurred while connecting to MongoDB Atlas...\n',err);
            client.close();
            return {success:false, err:"Fail to connect to database"};
        }
         console.log('Connected...');
        let collection = client.db("comp4770").collection("Level");
        collection.findOne({levelID:id}, function(err, level) {});
        client.close();
    });
    return {success:true, levelName:level.levelName, levelID:level.levelID,
        levelObjects:level.levelObjects};
}

let search = function(name) { //used for the level search
    MongoClient.connect(dburl, function(err, client) {
        if(err) {
            console.log('Error occurred while connecting to MongoDB Atlas...\n',err);
            client.close();
            return {success:false, err:"Fail to connect to database"};
        }
        console.log('Connected...');
        let collection = client.db("comp4770").collection("Level");
        // to do - actual search
        let stream = collection.find({levelName:name}).stream();
        stream.on("data", function(item) {});
        stream.on("end", function() {});
        client.close();
    });
    return {success:true, searchData:stream};
}

// won't let me name function 'delete' ?
let remove = function(id) { //delete level from db - player wants to delete their level
    MongoClient.connect(dburl, function(err, client) {
        if(err) {
            console.log('Error occurred while connecting to MongoDB Atlas...\n',err);
            client.close();
            return {success:false, err:"Fail to connect to database"};
        }
        console.log('Connected...');
        let collection = client.db("comp4770").collection("Level");
        collection.remove({levelID:id}, {w:1}, function(err, result) {});
        client.close();
    });
    return {success:true};
}
