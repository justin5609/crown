"use strict";
export { search, save };
let highscoreID, levelID, userID, time;

let MongoClient = require('mongodb').MongoClient;
let dburl = "mongodb+srv://admin:tWpwGPRe8aftswu2@cluster0-dyjn6.mongodb.net/test?retryWrites=true";

let save = function(userID, levelID, time) { //save highscore
    MongoClient.connect(dburl, function(err, client) {
        if(err) {
            console.log('Error occurred while connecting to MongoDB Atlas...\n',err);
            client.close();
            return {success:false, err:"Fail to connect to database"};
        }
        console.log('Connected...');
        let collection = client.db("comp4770").collection("Highscore");
            
        // first hs being saved? - first hs will have id 1
        let highscoreID = 1;
        // get last highscoreID, highest in db, -1 for descending order, take first (highest)
        collection.find({}, function(err, lastHS) {}).sort({highscoreID:-1}).limit(1);
        // increment it for the new hs - if lastHS exists, otherwise first hs and id=1
        if (lastHS !== null) {
            highscoreID = lastHS.highscoreID + 1;
        }

        // to do - add sort modifier and trim to top x scores
        collection.insert({userID:userID},
            {$addToSet:{time:time}},
            {$addToSett:{levelID:levelID}},
            {$addToSet:{highscoreID:highscoreID}});
        client.close();
    });
    // this may be changed to something more meaningful later
    return {success:true};
}

let search = function(ID) {//used for the highscore search
    MongoClient.connect(dburl, function(err, client) {
        if(err) {
            console.log('Error occurred while connecting to MongoDB Atlas...\n',err);
            client.close();
            return {success:false, err:"Fail to connect to database"};
        }
        console.log('Connected...');
        let collection = client.db("comp4770").collection("Highscore");
        // to do - actual search
        let scores = collection.find().toArray(function(err, items) {});
        client.close();
        return {success:true, searchData:scores};
    });
}