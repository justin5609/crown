window.onload=function(){

    // get canvas related references
    var canvas=document.getElementById("canvas");
    var ctx=canvas.getContext("2d");
    var BB=canvas.getBoundingClientRect();
    var offsetX=BB.left;
    var offsetY=BB.top;
    var WIDTH = canvas.width;
    var HEIGHT = canvas.height;
    p = 0; // some offset
    ctx.font = "30px Arial";
    ctx.strokeStyle ='grey';
    ctx.lineWidth=2;
    let bwidth = canvas.width;
    let bheight = canvas.height;
    let cellwidth = 70;
    let cellheight = 70;
    var isDragging = false;
    // drag related variables
    var dragok = false;
    var startX;
    var startY;
    // listen for mouse events
    canvas.onmousedown = handleMouseDown;
    canvas.onmouseup = handleMouseUp;
    canvas.onmousemove = handleMouseMove;
    canvas.mouseout = handleMouseOut;
    // clear the canva
    let players = []
    players.push({img:'nano.jpg',x:0,y:0,width:70,height:70});

    draw();
    function make_base(img,x,y,w,h){
        base_image = new Image();
        base_image.src = img;
        base_image.onload = function(){
        ctx.drawImage(base_image,x,y,w,h);
        }
    }
    
    function player(img1,x,y,w,h){
        base_image1 = new Image();
        base_image1.src = img1;
        base_image1.onload = function(){
        ctx.drawImage(base_image1, x, y, w, h);
        return base_image1;
        }
    }
    
       // clear the canvas
    function clear() {
        ctx.clearRect(0, 0, WIDTH, HEIGHT);
    }
    
    function drawBackground(){
        clear();
    }
       // redraw the scene
    function draw() {
        clear();
        //ctx.fillStyle = "#FAF7F8";
        make_base('OverWorld.png',70,0,70,70);
        player('nano.jpg',0,0,70,70);
        
        for(var i=0;i<players.length;i++){
            var r1=players[i];
            player(r1.img,r1.x,r1.y,r1.width,r1.height);          
        }
        
        for(var x = 0; x <= bwidth; x += cellwidth){
            ctx.moveTo(x, p);
            ctx.lineTo(x, bwidth)
            ctx.stroke();
        }
           
        for(var y = 0; y <= bwidth; y += cellwidth){
            ctx.moveTo(p, y);
            ctx.lineTo(bwidth, y)
            ctx.stroke();
           }
       }

    function handleMouseDown(e){
        canMouseX=parseInt(e.clientX-offsetX);
        canMouseY=parseInt(e.clientY-offsetY);
        // set the drag flag
        let background = new Image();
        background.src = 'OverWorld.png';
        if(canMouseX>70 && canMouseX<70+70 && canMouseY>0 && canMouseY<0+70){
            //alert('here');
            background.onload = function(){
            ctx.drawImage(background,0,0);
            ctx.globalAlpha = 0.4;
            }    
        draw();
        }
        isDragging=true;
      }
  
    function handleMouseUp(e){
        canMouseX=parseInt(e.clientX-offsetX);
        canMouseY=parseInt(e.clientY-offsetY);
        // clear the drag flag
        isDragging=false;
      }
  
    function handleMouseOut(e){
        canMouseX=parseInt(e.clientX-offsetX);
        canMouseY=parseInt(e.clientY-offsetY);
        // user has left the canvas, so clear the drag flag
        //isDragging=false;
      }
  
    function handleMouseMove(e){
        canMouseX=parseInt(e.clientX-offsetX);
        canMouseY=parseInt(e.clientY-offsetY);
        // if the drag flag is set, clear the canvas and draw the image
        if(isDragging){
            //draw();
            ctx.clearRect(0,0,bwidth,bheight);
            players.push({img:'nano.jpg',x:0,y:0,width:70,height:70});
            //ctx.drawImage('',canMouseX-70/2,canMouseY-70/2,70,70);
            
            //ctx.clearRect(0,0,bwidth,bheight);
            for(var i=0;i<players.length;i++){
                aa = players[i];
                make_base('OverWorld.png',0,0,600,500);
                player(aa.img,canMouseX-70/2,canMouseY-70/2,70,70);
            }
            ctx.clearRect(0,0,bwidth,bheight);
            draw();
        }
        //players.push({img:'nano.jpg',x:0,y:0,width:70,height:70});
      }

}; // end $(function(){}