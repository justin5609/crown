var express = require('express');
let MongoClient = require('mongodb').MongoClient;
let hash = require('object-hash');
let nodemailer = require('nodemailer');
let dburl = "mongodb://localhost:27017";

// emailer for password recovery - sends email from this address
let emailer = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'crownemailer@gmail.com',
      pass: 'K7ca8eumzZkzuVpmB4HgAAjW8lBcwh2U'
    }
});

var app = express();
var serv = require('http').Server(app);

app.get('/', function(req, res){ 
    res.sendFile(__dirname + '/client/index.html');
    console.log(__dirname);
});
app.use('/client',express.static(__dirname + '/client'));

serv.listen(2000);

var io = require('socket.io')(serv, {});
io.sockets.on('connection', function(socket) {
    console.log('socket connection');

    // login - existing account
    socket.on('loginAttempt', function (data) {
        console.log(data.username);
        let username = data.username;
        let password = data.password;
        let client = new MongoClient(dburl, { useNewUrlParser: true });
        client.connect(function (err, client) {
            if (err) {
                console.log('Error occurred while connecting to MongoDB Atlas...\n', err);
                socket.emit('loginVerif', { success: false, err: "Fail to connect to db" });
            } else {
                console.log('Connected...');
                let collection = client.db("local").collection("User");

                // to do - login
                collection.findOne({ "userID": username }, function (err, user) {
                    console.log(user);
                    if (user === null) {
                        // this may be changed to return something more useful
                        socket.emit('loginVerif', { success: false, userID:null, err: "Invalid username" });
                    } else {
                        //encrypt password before authentication
                        password = encrypt(data.password);
                        if (password !== user.password) {
                            // this may be changed to something more useful
                            socket.emit('loginVerif', { success: false, err: "Invalid password" });
                        } else {
                            // to do - handle successful login
                            socket.emit('loginVerif', { success: true, user:user });
                        }
                    }
                });
            }
        });
    });

    // signup - new acount
    socket.on('SignUpAttempt', async function (data) {
        console.log(data.username);
        console.log(data.password); // consider removing this log
        console.log(data.email);
        let username = data.username;
        let password = data.password;
        let email = data.email;
        // sign user up, log user in, return user to client - signup handles login

        let client = new MongoClient(dburl, { useNewUrlParser: true });
        client.connect(async function (err, client) {
            if (err) {
                console.log('Error occurred while connecting to MongoDB Atlas...\n', err);
                socket.emit('SignUpVerif', { success: false, err: "Fail to connect to db" });
            } else {
                console.log('Connected...');
                let collection = client.db("local").collection("User");

                // to do - signup
                // check if user exists
                collection.findOne({ "userID": username }, async function (err, user) {
                    if (user !== null) {
                        // if user can be found, it exists - username taken
                        socket.emit('SignUpVerif', { success: false, userID: null, err: "Username is unavailable" });
                    } else {
                        // check if email exists - needed for password retrieval
                        collection.findOne({ "email": email }, async function (err, addr) {
                            if (addr !== null) {
                                // if email can be found, it exists - email taken
                                socket.emit('SignUpVerif', { success: false, err: "Email already in use" });
                            } else {
                                // copy unencrypted password to use for login - otherwise it is encrypted twice and not work
                                //let upassword = password;

                                // password checks (contains no ; and length, etc.) will be done client side?
                                // encrypt password
                                password = encrypt(password);

                                // properties of user to save
                                // userID       password    storyID     createdLevels   achievments     settings    saveState
                                // given        given       increment   empty           initial         initial     initial

                                // create empty array for createdLevels - user hasn't created any yet
                                let levels = [];
                                // ~~~~~~~~~~~to do - achievements, settings, savestate~~~~~~~
                                // null values as placeholders
                                let achievements = null;
                                let settings = {soundOn:true, musicOn:true, keyBinds:[65, 68, 87, 83, 32, 69, 81, 27]};
                                //0 campaign levels completed
                                let saveState = 0;
                                //left right up down atk act weap pause
                                // save newly created user
                                collection.insertOne({
                                    "userID": username, "password": password,
                                    "email": email, "createdLevels": levels,
                                    "achievements": achievements,
                                    "settings": settings, "saveState": saveState
                                },
                                    function (err, result) {
                                        // ops[0] is the user
                                        console.log(result.ops[0]);
                                        socket.emit('SignUpVerif', { success: true, user: result.ops[0] });
                                    }
                                );
                            }
                        });
                    }
                });
            }
        });
    });

    // password recovery - recover password by email
    socket.on('passwordRecovery', async function (data) {
        console.log(data.userID);
        console.log(data.email);
        let userID = data.userID;
        let email = data.email;

        // random alpha-numeric password with length 16
        let password = "";
        let charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        for (var i = 0, n = charset.length; i < 16; ++i) {
            password += charset.charAt(Math.floor(Math.random() * n));
        }
        // encrypt password - password to email, newPassword to db
        let newPassword = encrypt(password);

        let client = new MongoClient(dburl, { useNewUrlParser: true });
        client.connect(function (err, client) {
            if (err) {
                console.log('Error occurred while connecting to MongoDB Atlas...\n', err);
                socket.emit('passwordRecoveryResponse', { success: false, err: "Fail to connect to db" });
            } else {
                console.log('Connected...');
                let collection = client.db("local").collection("User");
                // check if user exists
                collection.findOne({ "userID": userID }, function (err, user) {
                    if (user == null) {
                        // user not found - can't recover account that doesn't exist
                        socket.emit('passwordRecoveryResponse', { success: false, userID: null, err: "Invlaid username" });
                    } else {
                        // check if email exists - needed for password retrieval
                        collection.findOne({ "email": email }, function (err, addr) {
                            console.log(addr.email);
                            if (addr.email !== email) {
                                // if email can be found, it exists - email taken
                                socket.emit('passwordRecoveryResponse', { success: false, err: "Invalid email" });
                            } else {
                                // update password
                                collection.updateOne({ "userID": userID }, { $set: { "password": newPassword } }, function (err, result) {
                                    // email stuff
                                    let mailOptions = {
                                        from: 'crownemailer@gmail.com',
                                        to: email,
                                        subject: 'Account Recovery',
                                        text: 'Hello, ' + userID + '. Your new password is: ' + password +
                                            '. To change your password log in and go to Settings > Player Profile > Change Password.'
                                    };
                                    // send email
                                    emailer.sendMail(mailOptions, function(err, info){
                                        if (err) {
                                          console.log(err);
                                        } else {
                                          console.log('Email sent: ' + info.response);
                                          socket.emit('passwordRecoveryResponse', { success: true});
                                        }
                                    });
                                });
                            }
                        });
                    }
                });
            }
        });
    });

    // save new highscore
    socket.on('saveHS', function(data){
        console.log(data.userID);
        console.log(data.levelID);
        console.log(data.time);
        // return confirmation
        socket.emit(HSSave(data.userID, data.levelID, data.time));
    });

    // search highscores
    socket.on('loadHS', function(data){
        console.log(data.levelID);
        // return search
        socket.emit(HSLoad(data.levelID));
    });

    // save level
    socket.on('saveLevel', function(data){
        console.log(data.userID);
        console.log(data.levelID);
        console.log(data.levelName);
        console.log(data.levelDesc);
        console.log(data.tileArray);
        // -----TO BE ADDED-----
        //console.log(data.indexOfCheckpoint1);
        //console.log(data.indxOfCheckpoint2);
        //console.log(data.timeLimit);
        //console.log(data.musicTrack);
        //console.log(data.shop);
        // return confirmation
        let userID = data.userID;
        let levelID = data.levelID;
        let tileArray = data.tileArray;
        let levelName = data.levelName;
        let levelDesc = data.levelDesc;
        let shop = null;
        
        let client = new MongoClient(dburl, { useNewUrlParser: true });
        client.connect(async function (err, client) {
            if (err) {
                console.log('Error occurred while connecting to MongoDB Atlas...\n', err);
                socket.emit('LevelSaveVerif', { success: false, err: "Fail to connect to database" });
            } else {
                console.log('Connected...');
                let collection = client.db("local").collection("Level");
                // to do - actual saving
                if (levelID !== null) {
                    //save as existing level being updated
                    collection.updateOne({ "levelID": levelID },{$set:{ "tileArray": tileArray,
                        "levelName":levelName, "levelDesc":levelDesc }}, function (err, level) {
                        if (err){
                            socket.emit('LevelSaveVerif', { success: false, err: "Failed to save level"});
                        }else{
                            //reset highscores for the level that was edited
                            let hsCollection = client.db("local").collection("Highscore");
                            hsCollection.deleteMany({"levelID":levelID}, function(){
                                let userCollection = client.db("local").collection("User");
                                userCollection.updateOne({"userID":userID, "createdLevels.levelID":levelID}, {$set:{ "createdLevels.$.tileArray": tileArray,
                                        "createdLevels.$.levelName":levelName, "createdLevels.$.levelDesc":levelDesc }}, function(err, createdResults){
                                            console.log(createdResults);
                                    socket.emit('LevelSaveVerif', { success: true});
                                });
                            });
                        }
                    });
                } else {
                    //save as new level - needs new level id

                    // first level being saved? - first level will have id 1
                    let levelID = 1;
                    let lastLevel = null;
                    // get last highscoreID, highest in db, -1 for descending order, take first (highest)
                    collection.find({}).sort({ "levelID": -1 }).limit(1).toArray(function (err, lastLevel) {
                        // results[0] is last level id
                        levelID = lastLevel[0].levelID + 1;
                        console.log(levelID);
                        // save level
                        collection.insertOne({
                            "levelID": levelID, "tileArray": tileArray, "levelName": levelName,
                            "levelDesc":levelDesc, "userID":userID, "shop": shop
                        },function (err, level) {
                            if (err) {
                                console.log('Error: ' + err);
                                socket.emit('LevelSaveVerif', { success: false, err: "Failed to save level" });
                            } else {
                                // update user.createdLevels with new level
                                // find user
                                let userCollection = client.db("local").collection("User");
                                userCollection.findOne({ "userID": userID }, function (err, user) {
                                    if (err) {
                                        console.log('Error: ' + err);
                                        socket.emit('LevelSaveVerif', { success: false, err: "Failed to save level" });
                                    } else {
                                        //$push treats the array as a stack, pinning the id at the end of the list
                                        //level.ops[0] is the level info that was just added
                                        userCollection.updateOne({ "userID": userID }, { $push: { "createdLevels": level.ops[0] } },
                                            function (err, updated) {
                                            if (err) {
                                                console.log('Error: ' + err);
                                                socket.emit('LevelSaveVerif', { success: false, err: "Failed to save level" });
                                            } else {
                                                //console.log(results);
                                                socket.emit('LevelSaveVerif', { success: true, level:level.ops[0] });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    });
                }
            }
        });
    });

    // load level
    socket.on('loadLevel', function (data) {
        console.log(data.levelID);
        //return level
        //socket.emit(levelLoad(levelID));
        let client = new MongoClient(dburl, { useNewUrlParser: true });
        client.connect(async function (err, client) {
            if (err) {
                console.log('Error occurred while connecting to MongoDB Atlas...\n', err);
                socket.emit('retrieveLevelResponse', { success: false, err: "Fail to connect to database" });
            } else {
                console.log('Connected...');
                let collection = client.db("local").collection("Level");
                collection.findOne({ "levelID":data.levelID }, function (err, level) {
                    if (level === null) {
                        socket.emit('retrieveLevelResponse', { success: false, err: "Fail to find level" });
                    } else {
                        console.log(level);
                        socket.emit('retrieveLevelResponse', {
                            success: true, levelName: level.levelName, levelDesc:level.levelDesc,
                            levelID:level.levelID, tileArray:level.tileArray, userID:level.userID
                        });
                    }
                });
            }
        });
    });

    // delete level
    socket.on('deleteLevel', function (data) {
        console.log(data.levelID);
        console.log(data.userID);
        let levelID = data.levelID;
        let userID = data.userID;
        let client = new MongoClient(dburl, { useNewUrlParser: true });
        client.connect(async function (err, client) {
            if (err) {
                console.log('Error occurred while connecting to MongoDB Atlas...\n', err);
                socket.emit('deleteLevelResponse', { success: false, err: "Fail to find level" });
            }
            console.log('Connected...');
            let collection = client.db("local").collection("Level");
            let userCollection = client.db("local").collection("User");
            collection.removeOne({ "levelID": levelID }, function (err, level) {
                console.log(level);
                userCollection.updateOne({"userID":userID}, {$pull:{createdLevels: {"levelID":levelID}}}, function(err, clevel){
                    console.log(clevel);
                    socket.emit('deleteLevelResponse', { success: true});
                });
            });
        });
    });

    // Return user's levels
    socket.on('playerLevelRequest', function (data) {
        console.log('Player Level Request');
        let client = new MongoClient(dburl, { useNewUrlParser: true });
        client.connect(function (err, client) {
            if (err) {
                console.log('Error occurred while connecting to MongoDB Atlas...\n', err);
                socket.emit('playerLevelResponse', { success: false, err: "Fail to connect to database" });
            } else {
                console.log('Connected...');
                let collection = client.db("local").collection("User");
                collection.findOne({ "userID": data.userID }, function (err, user) {
                    console.log(user.createdLevels);
                    socket.emit('playerLevelResponse', { success: true, levelList: user.createdLevels });
                });
            }
        });
    });

    // search level
    socket.on('searchLevel', function (data) {
        console.log(data.searchFieldData);
        //return search
        //socket.emit(levelSearch(searchFieldData));
        let client = new MongoClient(dburl, { useNewUrlParser: true });
        client.connect(function (err, client) {
            if (err) {
                console.log('Error occurred while connecting to MongoDB Atlas...\n', err);
                socket.emit('playerLevelResponse', { success: false, err: "Fail to connect to database" });
            } else {
                console.log('Connected...');
                let collection = client.db("local").collection("Level");
                // to do - actual search
                // $regex searches for values that contain string, $option i makes the search non-case sensitive
                // sort by levelname? and take result as a list via toArray
                collection.find({ "levelName": { "$regex": data.searchFieldData, "$options": "i" } }
                ).sort({ "levelName": -1 }).toArray(function (err, results) {
                    socket.emit('playerLevelResponse', { success: true, levelList: results });
                    console.log(results);
                });
            }
        });
    });

    // random level
    socket.on('retrieveRandomLevel', function (data) {
        let client = new MongoClient(dburl, { useNewUrlParser: true });
        client.connect(function (err, client) {
            if (err) {
                console.log('Error occurred while connecting to MongoDB Atlas...\n', err);
                socket.emit('retrieveLevelResponse', { success: false, err: "Fail to connect to database" });
            } else {
                console.log('Connected...');
                let collection = client.db("local").collection("Level");
                // to do - actual search
                // $regex searches for values that contain string, $option i makes the search non-case sensitive
                // sort by levelname? and take result as a list via toArray
                collection.countDocuments({levelID: {$gt:0}}, function(err, num){
                    if (err){
                        console.log('Error occurred while connecting to MongoDB Atlas...\n', err);
                        socket.emit('retrieveLevelResponse', { success: false, err: "Fail to find level" });
                    } else {
                        // first level is 0 id - not an actual level - subtract then add 1 to avoid this level
                        let rndIndex = Math.floor(Math.random()*(num-1)) + 1;
                        collection.find({}).toArray(function(err, results){
                            if (err){
                                console.log('Error occurred while connecting to MongoDB Atlas...\n', err);
                                socket.emit('retrieveLevelResponse', { success: false, err: "Fail to find level" });
                            } else {
                                let level = results[rndIndex];
                                socket.emit('retrieveLevelResponse', {
                                    success: true, levelName: level.levelName, levelDesc:level.levelDesc,
                                    levelID:level.levelID, tileArray:level.tileArray, userID:level.userID
                                });
                            }
                        });
                    }
                });
            }
        });
    });

    // save campaign progress
    socket.on('saveCampaign', function (data) {
        console.log(data.userID);
        console.log(data.saveState);
        userID = data.userID;
        saveState = data.saveState;
        let client = new MongoClient(dburl, { useNewUrlParser: true });
        client.connect(function (err, client) {
            if (err) {
                console.log('Error occurred while connecting to MongoDB Atlas...\n', err);
                socket.emit('saveCampaignResponse', { success: false, err: "Fail to connect to database" });
            } else {
                console.log('Connected...');
                let collection = client.db("local").collection("User");
                collection.updateOne({"userID":userID}, {$set:{"saveState":saveState}}, function(err, result){
                    if (err){
                        socket.emit('saveCampaignResponse', { success: false, err: "Fail to save campaign progress" });
                    } else {
                        socket.emit('saveCampaignResponse', { success: true });
                        console.log(result);
                    }
                });
            }
        });
    });

    //Saving A Highscore
    socket.on('saveHighscore', function (data) {
        console.log(data.levelID);
        console.log(data.userID);
        console.log(data.time);
        console.log(data.score);
        userID = data.userID;
        levelID = data.levelID;
        time = data.time;
        score = data.score;
        let client = new MongoClient(dburl, { useNewUrlParser: true });
        client.connect(function (err, client) {
            if (err) {
                console.log('Error occurred while connecting to MongoDB Atlas...\n', err);
                socket.emit('saveHighScoreResponse', { success: false, err: "Fail to connect to database" });
            } else {
                console.log('Connected...');
                let collection = client.db("local").collection("Highscore");
                collection.insertOne({
                    "userID": userID, "levelID": levelID, "time": time, "score": score
                }, function (err, result) {
                    if (err) {
                        socket.emit('saveHighscoreResponse', { success: false, err: "Failed to save high score." });
                    } else {
                        socket.emit('saveHighscoreResponse', { success: true });
                        console.log(result);
                    }
                })
            }
        });
    });

    socket.on('loadHighscores', function (data) {
        console.log(data.levelID);
        let client = new MongoClient(dburl, { useNewUrlParser: true });
        client.connect(function (err, client) {
            if (err) {
                console.log('Error occurred while connecting to MongoDB Atlas...\n', err);
                socket.emit('loadHighscoresResponse', { success: false, err: "Fail to connect to database" });
            } else {
                console.log('Connected...');
                let collection = client.db("local").collection("Highscore");
                collection.find({ "levelID":data.levelID} 
                ).sort({ "time": 1 }).toArray(function (err, results) {
                    if (err) {
                        socket.emit('loadHighscoresResponse', { success: false, err: "Failed to load high scores." });
                    }
                    socket.emit('loadHighscoresResponse', { success: true, highscore: results });
                    console.log(results);
                });
            }
        });
    });

    // update password - used for changing password
    socket.on('passwordUpdate', function(data){
        console.log(data.userID);
        console.log(data.newPassword);
        let userID = data.userID;
        let newPassword = data.newPassword; //consider removing this log

        newPassword = encrypt(newPassword);

        let client = new MongoClient(dburl, { useNewUrlParser: true });
        client.connect(function (err, client) {
            if (err) {
                console.log('Error occurred while connecting to MongoDB Atlas...\n', err);
                socket.emit('passwordUpdateResponse', { success: false, err: "Fail to connect to database" });
            }
            console.log('Connected...');
            let collection = client.db("local").collection("User");
            collection.update({"userID":userID}, {$set:{"password":newPassword}}, function (err, results){
                socket.emit('passwordUpdateResponse', {success: true});
                console.log(results);
            });
        });
    });

    socket.on('settingsUpdate', function(data){
        console.log(data.settings.musicOn);
        console.log(data.settings.soundOn);
        console.log(data.settings.keyBinds);
        let userID = data.userID;
        let settings = data.settings;
        let client = new MongoClient(dburl, { useNewUrlParser: true });
        client.connect(function (err, client) {
            if (err) {
                console.log('Error occurred while connecting to MongoDB Atlas...\n', err);
                socket.emit('settingsUpdateResponse', { success: false, err: "Fail to connect to database" });
            }
            console.log('Connected...');
            let collection = client.db("local").collection("User");
            collection.updateOne({"userID":userID}, {$set:{settings:settings}}, function (err, results){
                if (err) {
                    socket.emit('settingsUpdateResponse', {success: false, err:"Failed to Save Settings"});
                } else {
                    socket.emit('settingsUpdateResponse', {success: true});
                }
            });
        });
    });
});

// password encryption
let encrypt = function(str) {
    // salt
    salt = "K7ca8eumzZkzuVpmB4HgAAjW8lBcwh2U";
    // take partial salt depending on length of original password
    // - means the salt will be different for different password lengths
    str = str + salt.slice(0,0-str.length);
    // hash
    str = hash(str);
    return str;
}
