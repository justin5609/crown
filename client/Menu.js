//Canvas + Default Variables
let menu_canvas = document.getElementById("myCanvas");
let canvasWidth = menu_canvas.width
let canvasHeight = menu_canvas.height
let menu_ctx = menu_canvas.getContext("2d");
let currentMenuState = 0;
let previousMenuState = 0;
let levelListPageIndex = 0;
let levelList = null;
let userID;
let settings = {
    musicOn: false,
    soundOn: false,
    keyBinds: [65, 68, 87, 83, 32, 69, 81, 27]
};
let createdLevels;
let sock = io();
let campaignBackgroundJS = new Image();
let storyProgress = 0;
campaignBackgroundJS.src = './client/assets/maxresdefault.jpg';
let menuBackgroundJS = new Image();

//Textfields
let usernameFieldLogin = document.getElementById('usernameFieldLogin');
let passwordFieldLogin = document.getElementById('passwordFieldLogin');
let usernameFieldSignup = document.getElementById('usernameFieldSignup');
let emailFieldSignUp = document.getElementById('emailFieldSignup');
let passwordFieldSignUp = document.getElementById('passwordFieldSignup');
let passwordConfirmFieldSignUp = document.getElementById('passwordConfirmFieldSignup');
let levelSearchField = document.getElementById('levelSearchField');
let forgotPasswordUsernameField = document.getElementById('forgotPasswordUsernameField');
let forgotPasswordEmailField = document.getElementById('forgotPasswordEmailField');
let changePasswordField = document.getElementById('changePasswordField');
let changePasswordConfirmField = document.getElementById('changePasswordConfirmField');

//Buttons for the menus
let loginButtons = [
    {
        x: canvasWidth / 2 - 390,
        y: canvasHeight / 2 + 100,
        height: 100,
        width: 180,
        text: 'Login'
    }, {
        x: canvasWidth / 2 - 190,
        y: canvasHeight / 2 + 100,
        height: 100,
        width: 180,
        text: 'Sign Up'
    }, {
        x: canvasWidth / 2 + 10,
        y: canvasHeight / 2 + 100,
        height: 100,
        width: 180,
        text: 'Forgot Password',
        wrap: true
    }, {
        x: canvasWidth / 2 + 210,
        y: canvasHeight / 2 + 100,
        height: 100,
        width: 180,
        text: 'Exit'
    }
];
let signUpButtons = [
    {
        x: canvasWidth / 2 - 190,
        y: canvasHeight / 2 + 100,
        height: 100,
        width: 180,
        text: 'Back'
    }, {
        x: canvasWidth / 2 + 10,
        y: canvasHeight / 2 + 100,
        height: 100,
        width: 180,
        text: 'Sign Up'
    }
];
let mainMenuButtons = [
    {
        x: canvasWidth / 2 - 150,
        y: canvasHeight / 10 - 15,
        height: 100,
        width: 300,
        text: 'Play'
    }, {
        x: canvasWidth / 2 - 150,
        y: 3 * canvasHeight / 10 - 5,
        height: 100,
        width: 300,
        text: 'Create'
    }, {
        x: canvasWidth / 2 - 150,
        y: 5 * canvasHeight / 10 + 5,
        height: 100,
        width: 300,
        text: 'Settings'
    }, {
        x: canvasWidth / 2 - 150,
        y: 7 * canvasHeight / 10 + 15,
        height: 100,
        width: 300,
        text: 'Log Out'
    }
];
let createMenuButtons = [
    {
        x: canvasWidth / 2 - 150,
        y: canvasHeight / 10 - 15,
        height: 100,
        width: 300,
        text: 'New Level'
    }, {
        x: canvasWidth / 2 - 150,
        y: 3 * canvasHeight / 10 - 5,
        height: 100,
        width: 300,
        text: 'Edit Level'
    }, {
        x: canvasWidth / 2 - 150,
        y: 5 * canvasHeight / 10 + 5,
        height: 100,
        width: 300,
        text: 'Back'
    }
];
let playMenuButtons = [
    {
        x: canvasWidth / 2 - 150,
        y: canvasHeight / 10 - 15,
        height: 100,
        width: 300,
        text: 'Campaign'
    }, {
        x: canvasWidth / 2 - 150,
        y: 3 * canvasHeight / 10 - 5,
        height: 100,
        width: 300,
        text: 'Search'
    }, {
        x: canvasWidth / 2 - 150,
        y: 5 * canvasHeight / 10 + 5,
        height: 100,
        width: 300,
        text: 'Random Level',
        wrap: true
    }, {
        x: canvasWidth / 2 - 150,
        y: 7 * canvasHeight / 10 + 15,
        height: 100,
        width: 300,
        text: 'Back'
    }
];
let settingsButtons = [
    {
        x: 100,
        y: 25,
        height: 80,
        width: 300,
        text: 'Back'
    }, {
        x: 880,
        y: 25,
        height: 80,
        width: 300,
        text: 'Player Profile',
        wrap: true
    }, {
        x: 150,
        y: 115,
        height: 70,
        width: 450,
        text: 'Music'
    }, {
        x: 880,
        y: 115,
        height: 70,
        width: 80,
        text: '',
        isEnabled: true
    }, {
        x: 150,
        y: 195,
        height: 70,
        width: 450,
        text: 'Game Sound'
    }, {
        x: 880,
        y: 195,
        height: 70,
        width: 80,
        text: '',
        isEnabled: false
    }, {
        x: 150,
        y: 275,
        height: 70,
        width: 300,
        text: 'Left'
    }, {
        x: 475,
        y: 275,
        height: 70,
        width: 103,
        text: 'A',
        isListening: false,
        currentKey: false
    }, {
        x: 150,
        y: 355,
        height: 70,
        width: 300,
        text: 'Right'
    }, {
        x: 475,
        y: 355,
        height: 70,
        width: 103,
        text: 'D',
        isListening: false,
        currentKey: false
    }, {
        x: 150,
        y: 435,
        height: 70,
        width: 300,
        text: 'Jump'
    }, {
        x: 475,
        y: 435,
        height: 70,
        width: 103,
        text: 'W',
        isListening: false,
        currentKey: false
    }, {
        x: 150,
        y: 515,
        height: 70,
        width: 300,
        text: 'Down'
    }, {
        x: 475,
        y: 515,
        height: 70,
        width: 103,
        text: 'S',       
        isListening: false,
        currentKey: false
    }, {
        x: 683,
        y: 275,
        height: 70,
        width: 300,
        text: 'Attack'
    }, {
        x: 1008,
        y: 275,
        height: 70,
        width: 102,
        text: '\'    \'',
        isListening: false,
        currentKey: false
    }, {
        x: 683,
        y: 355,
        height: 70,
        width: 300,
        text: 'Action'
    }, {
        x: 1008,
        y: 355,
        height: 70,
        width: 102,
        text: 'E',
        isListening: false,
        currentKey: false
    }, {
        x: 683,
        y: 435,
        height: 70,
        width: 300,
        text: 'Weapon Toggle',
        wrap: true
    }, {
        x: 1008,
        y: 435,
        height: 70,
        width: 102,
        text: 'Q',
        isListening: false,
        currentKey: false
    }, {
        x: 683,
        y: 515,
        height: 70,
        width: 300,
        text: 'Pause'
    }, {
        x: 1008,
        y: 515,
        height: 70,
        width: 102,
        text: 'Esc',       
        isListening: false,
        currentKey: false
    }
];
let searchedLevelButtons = [
    {
        x: 100,
        y: 20,
        height: 80,
        width: 200,
        text: 'Back'
    }, {
        x: 980,
        y: 20,
        height: 80,
        width: 200,
        text: 'Search'
    }, {
        x: 100,
        y: 120,
        height: 80,
        width: 1080,
        text: '',
        levelID: '',
        containsLevel: false
    }, {
        x: 100,
        y: 220,
        height: 80,
        width: 1080,
        text: '',
        levelID: '',
        containsLevel: false
    }, {
        x: 100,
        y: 320,
        height: 80,
        width: 1080,
        text: '',
        levelID: '',
        containsLevel: false
    }, {
        x: 100,
        y: 420,
        height: 80,
        width: 1080,
        text: '',
        levelID: '',
        containsLevel: false
    }, {
        x: 100,
        y: 520,
        height: 80,
        width: 1080,
        text: '',
        levelID: '',
        containsLevel: false
    }, {
        x: 100,
        y: 620,
        height: 80,
        width: 200,
        text: 'Previous'
    }, {
        x: 980,
        y: 620,
        height: 80,
        width: 200,
        text: 'Next',
        numLevels: 0
    }
];
let editLevelButtons = [
    {
        x: 100,
        y: 20,
        height: 80,
        width: 200,
        text: 'Back'
    }, {
        x: 100,
        y: 120,
        height: 80,
        width: 740,
        text: '',
        levelID: '',
        containsLevel: false
    }, {
        x: 850,
        y: 120,
        height: 80,
        width: 160,
        text: 'Edit',
    }, {
        x: 1020,
        y: 120,
        height: 80,
        width: 160,
        text: 'Delete',
    }, {
        x: 100,
        y: 220,
        height: 80,
        width: 740,
        text: '',
        levelID: '',
        containsLevel: false
    }, {
        x: 850,
        y: 220,
        height: 80,
        width: 160,
        text: 'Edit',
    }, {
        x: 1020,
        y: 220,
        height: 80,
        width: 160,
        text: 'Delete',
    }, {
        x: 100,
        y: 320,
        height: 80,
        width: 740,
        text: '',
        levelID: '',
        containsLevel: false
    }, {
        x: 850,
        y: 320,
        height: 80,
        width: 160,
        text: 'Edit',
    }, {
        x: 1020,
        y: 320,
        height: 80,
        width: 160,
        text: 'Delete',
    }, {
        x: 100,
        y: 420,
        height: 80,
        width: 740,
        text: '',
        levelID: '',
        containsLevel: false
    }, {
        x: 850,
        y: 420,
        height: 80,
        width: 160,
        text: 'Edit',
    }, {
        x: 1020,
        y: 420,
        height: 80,
        width: 160,
        text: 'Delete',
    }, {
        x: 100,
        y: 520,
        height: 80,
        width: 740,
        text: '',
        levelID: '',
        containsLevel: false
    }, {
        x: 850,
        y: 520,
        height: 80,
        width: 160,
        text: 'Edit',
    }, {
        x: 1020,
        y: 520,
        height: 80,
        width: 160,
        text: 'Delete',
    }, {
        x: 100,
        y: 620,
        height: 80,
        width: 200,
        text: 'Previous'
    }, {
        x: 980,
        y: 620,
        height: 80,
        width: 200,
        text: 'Next',
        numLevels: 0
    }
];
let campaignButtons = [
    {
        x: 20,
        y: 620,
        height: 80,
        width: 120,
        text: 'Back',
        isUnlocked: true
    }, {
        x: 120,
        y: 35,
        height: 40,
        width: 60,
        text: '',
        levelID: 1,
        isUnlocked: true,
        isCompleted: false
    }, {
        x: 355,
        y: 110,
        height: 40,
        width: 60,
        text: '',
        levelID: 2,
        isUnlocked: false,
        isCompleted: false
    }, {
        x: 540,
        y: 175,
        height: 40,
        width: 60,
        text: '',
        levelID: 3,
        isUnlocked: false,
        isCompleted: false
    }, {
        x: 680,
        y: 315,
        height: 40,
        width: 60,
        text: '',
        levelID: 4,
        isUnlocked: false,
        isCompleted: false
    }, {
        x: 976,
        y: 315,
        height: 40,
        width: 60,
        text: '',
        levelID: 5,
        isUnlocked: false,
        isCompleted: false
    }, {
        x: 1100,
        y: 315,
        height: 40,
        width: 60,
        text: '',
        levelID: 6,
        isUnlocked: false,
        isCompleted: false
    }, {
        x: 976,
        y: 500,
        height: 40,
        width: 60,
        text: '',
        levelID: 7,
        isUnlocked: false,
        isCompleted: false
    }, {
        x: 840,
        y: 640,
        height: 40,
        width: 60,
        text: '',
        levelID: 8,
        isUnlocked: false,
        isCompleted: false
    }, {
        x: 720,
        y: 580,
        height: 40,
        width: 60,
        text: '',
        levelID: 9,
        isUnlocked: false,
        isCompleted: false
    }, {
        x: 712,
        y: 450,
        height: 60,
        width: 80,
        text: '',
        levelID: 10,
        isUnlocked: false,
        isCompleted: false
    }
]
let levelPageButtons = [
    {
        x: 50,
        y: 20,
        height: 80,
        width: 250,
        text: 'Back'
    }, {
        x: 320,
        y: 20,
        height: 80,
        width: 400,
        text: 'Creators Name',
        font: '25pt Impact'
    }, {
        x: 50,
        y: 220,
        height: 250,
        width: 300,
        text: ''
    }, {
        x: 50,
        y: 120,
        height: 80,
        width: 670,
        text: 'Level Name',
        levelID: '',
    }, {
        x: 440,
        y: 265,
        height: 160,
        width: 200,
        text: 'Play',
        font: '60pt Impact',
        tileArray: null,
    }, {
        x: 50,
        y: 490,
        height: 210,
        width: 670,
        text: 'Description',
        font: '25pt Impact'
    }
]
let forgotPasswordButtons = [
    {
        x: canvasWidth / 2 - 190,
        y: canvasHeight / 2 + 100,
        height: 100,
        width: 180,
        text: 'Back'
    }, {
        x: canvasWidth / 2 + 10,
        y: canvasHeight / 2 + 100,
        height: 100,
        width: 180,
        text: 'Send'
    }
];
let playerProfileButtons = [
    {
        x: 100,
        y: 25,
        height: 100,
        width: 300,
        text: 'Back'
    }, {
        x: 880,
        y: 25,
        height: 100,
        width: 300,
        text: 'Change Password',
        wrap: true
    }
];
let changePasswordButtons = [
    {
        x: canvasWidth / 2 - 190,
        y: canvasHeight / 2 + 100,
        height: 100,
        width: 180,
        text: 'Back'
    }, {
        x: canvasWidth / 2 + 10,
        y: canvasHeight / 2 + 100,
        height: 100,
        width: 180,
        text: 'Change'
    }
]
//FUNCTIONS FOR DRAWING TO THE CANVAS
//Clears the canvas
function clearCanvas() {
    menu_ctx.clearRect(0, 0, canvasWidth, canvasHeight);
}

//Function used for drawing any button.
function CreateButton(menuButton) {
    radius = 10;
    menu_ctx.beginPath();
    menu_ctx.moveTo(menuButton.x + radius, menuButton.y);
    menu_ctx.lineTo(menuButton.x + menuButton.width - radius, menuButton.y);
    menu_ctx.quadraticCurveTo(menuButton.x + menuButton.width, menuButton.y, menuButton.x + menuButton.width, menuButton.y + radius);
    menu_ctx.lineTo(menuButton.x + menuButton.width, menuButton.y + menuButton.height - radius);
    menu_ctx.quadraticCurveTo(menuButton.x + menuButton.width, menuButton.y + menuButton.height, menuButton.x + menuButton.width - radius, menuButton.y + menuButton.height);
    menu_ctx.lineTo(menuButton.x + radius, menuButton.y + menuButton.height);
    menu_ctx.quadraticCurveTo(menuButton.x, menuButton.y + menuButton.height, menuButton.x, menuButton.y + menuButton.height - radius);
    menu_ctx.lineTo(menuButton.x, menuButton.y + radius);
    menu_ctx.quadraticCurveTo(menuButton.x, menuButton.y, menuButton.x + radius, menuButton.y);
    if (menuButton.isUnlocked === false || menuButton.isEnabled === false || menuButton.containsLevel === false || menuButton.isListening) {
        menu_ctx.fillStyle = '#BFBFBF'
        menu_ctx.fillStyle = 'rgba(255, 100, 100, 1.0)'; //Color of Locked Levels
    } else if (menuButton.isUnlocked === true) {
        if (menuButton.isCompleted === true) {
            menu_ctx.fillStyle = '#FFFFFF';
            menu_ctx.fillStyle = 'rgba(100, 255, 100, 1.0)'; // Color of Completed Levels 
        } else {
            menu_ctx.fillStyle = '#FFFFFF';
            menu_ctx.fillStyle = 'rgba(100, 100, 255, 1.0)'; // Color of Unlocked Levels
        }
    } else {
        menu_ctx.fillStyle = '#FFFFFF';
        menu_ctx.fillStyle = 'rgba(255,255,225,0.6)'; // Color of Regular Buttons 
    }
    menu_ctx.fill();
    menu_ctx.lineWidth = 2;
    menu_ctx.strokeStyle = '#757BE8'; //Color Of Box Outline
    menu_ctx.stroke();
    menu_ctx.closePath();
    menu_ctx.fillStyle = '#000000'; //Color of Text
    menu_ctx.textAlign = 'center';
    if (menuButton.font != undefined) {
        menu_ctx.font = menuButton.font;
        menu_ctx.fillText(menuButton.text, menuButton.x + menuButton.width / 2, menuButton.y + (menuButton.height * .7));
    } else {
        if (menuButton.wrap === true) {
            let name = menuButton.text.split(' ');
            menu_ctx.font = '30pt Impact';
            menu_ctx.fillText(name[0], menuButton.x + menuButton.width / 2, menuButton.y + (menuButton.height * .45));
            menu_ctx.fillText(name[1], menuButton.x + menuButton.width / 2, menuButton.y + (menuButton.height * .85));
        } else {
            menu_ctx.font = '40pt Impact';
            menu_ctx.fillText(menuButton.text, menuButton.x + menuButton.width / 2, menuButton.y + (menuButton.height * .7));
        }
    }
}

//Draws the menu and adds listeners
function drawMenu(buttonList, newMenuState) {
    clearCanvas()
    if (parseInt(currentMenuState) != parseInt(newMenuState)) {
        previousMenuState = currentMenuState;
        currentMenuState = newMenuState;
    }
    menu_ctx.drawImage(menuBackgroundJS, 0, 0, canvasWidth, canvasHeight);
    removeListeners();
    switch (currentMenuState) {
        case 0:
            menu_ctx.fillStyle = '#000000'; //Color of Text
            menu_ctx.textAlign = 'center';
            menu_ctx.font = '100pt Castellar';
            menu_ctx.fillText('Crown', 640, 200);
            usernameFieldLogin.style = 'font-size:26px; position: absolute; left: 496px; top: 300px; width:300px; height:30px';
            passwordFieldLogin.style = 'font-size:26px; position: absolute; left: 496px; top: 360px; width:300px; height:30px';
            menu_canvas.addEventListener('click', loginListener);
            break;
        case 1:
            usernameFieldSignup.style = 'font-size:26px; position: absolute; left: 496px; top: 200px; width:300px; height:30px';
            emailFieldSignup.style = 'font-size:26px; position: absolute; left: 496px; top: 260px; width:300px; height:30px';
            passwordFieldSignup.style = 'font-size:26px; position: absolute; left: 496px; top: 320px; width:300px; height:30px';
            passwordConfirmFieldSignup.style = 'font-size:26px; position: absolute; left: 496px; top: 380px; width:300px; height:30px';
            menu_canvas.addEventListener('click', signUpListener);
            break;
        case 2:
            menu_canvas.addEventListener('click', mainListener);
            break;
        case 3:
            menu_canvas.addEventListener('click', playListener);
            break;
        case 4:
            menu_canvas.addEventListener('click', campaignListener);
            menu_ctx.drawImage(campaignBackgroundJS, 0, 0, canvasWidth, canvasHeight);
            break;
        case 5:
            levelSearchField.style = 'font-size:60px; position: absolute; left: 320px; top: 28px; width:655px; height:74px'
            menu_canvas.addEventListener('click', searchLevelMenuListener);
            break;
        case 6:
            menu_canvas.addEventListener('click', levelPageListener);
            drawLeaderboard();
            break;
        case 7:
            menu_canvas.addEventListener('click', createListener);
            break;
        case 8:
            menu_canvas.addEventListener('click', editLevelMenuListener);
            break;
        case 9:
            menu_canvas.addEventListener('click', settingsListener);
            break;
        case 10:
            forgotPasswordUsernameField.style = 'font-size:26px; position: absolute; left: 496px; top: 300px; width:300px; height:30px';
            forgotPasswordEmailField.style = 'font-size:26px; position: absolute; left: 496px; top: 360px; width:300px; height:30px';
            menu_canvas.addEventListener('click', forgotPasswordListener);
            break;
        case 11:
            menu_canvas.addEventListener('click', playerProfileListener);
            break;
        case 12:
            changePasswordField.style = 'font-size:26px; position: absolute; left: 496px; top: 300px; width:300px; height:30px';
            changePasswordConfirmField.style = 'font-size:26px; position: absolute; left: 496px; top: 360px; width:300px; height:30px';
            menu_canvas.addEventListener('click', changePasswordListener);
            break;
    }
    for (i = 0; i < buttonList.length; i++) {
        CreateButton(buttonList[i]);
    }
}

//Draws leaderboard for Level Page
function drawLeaderboard() {
    sock.emit('leaderboardRequest', { levelID: levelPageButtons[3].levelID });
}

//FUNCTIONS FOR LISTENING TO CLICKS ON THE CANVAS
//Removes all menu listeners
function removeListeners() {
    menu_canvas.removeEventListener('click', mainListener);
    menu_canvas.removeEventListener('click', playListener);
    menu_canvas.removeEventListener('click', createListener);
    menu_canvas.removeEventListener('click', settingsListener);
    menu_canvas.removeEventListener('click', loginListener);
    menu_canvas.removeEventListener('click', searchLevelMenuListener);
    menu_canvas.removeEventListener('click', editLevelMenuListener);
    menu_canvas.removeEventListener('click', signUpListener);
    menu_canvas.removeEventListener('click', levelPageListener);
    menu_canvas.removeEventListener('click', campaignListener);
    menu_canvas.removeEventListener('click', forgotPasswordListener);
    menu_canvas.removeEventListener('click', playerProfileListener);
    menu_canvas.removeEventListener('click', changePasswordListener);
}

//Calculates mouse position for use in listeners
function getMousePos(canvas, event) {
    let rect = canvas.getBoundingClientRect();
    return {
        x: event.clientX - rect.left,
        y: event.clientY - rect.top
    };
}

//Check mouse position against button areas
function isInside(pos, rect) {
    return pos.x > rect.x && pos.x < rect.x + rect.width && pos.y < rect.y + rect.height && pos.y > rect.y;
}

function loginListener(evt) {
    let mousePos = getMousePos(menu_canvas, evt);
    if (isInside(mousePos, loginButtons[0])) {
        sock.emit('loginAttempt', { username: usernameFieldLogin.value, password: passwordFieldLogin.value });
    } else if (isInside(mousePos, loginButtons[1])) {
        usernameFieldLogin.value = '';
        usernameFieldLogin.style.display = 'none';
        passwordFieldLogin.value = '';
        passwordFieldLogin.style.display = 'none';
        drawMenu(signUpButtons, 1);
    } else if (isInside(mousePos, loginButtons[2])) {
        usernameFieldLogin.value = '';
        usernameFieldLogin.style.display = 'none';
        passwordFieldLogin.value = '';
        passwordFieldLogin.style.display = 'none';
        drawMenu(forgotPasswordButtons, 10);
    } else if (isInside(mousePos, loginButtons[3])) {
        alert('Exit buttoned pressed');
        //close();
    }
}

function signUpListener(evt) {
    let mousePos = getMousePos(menu_canvas, evt);
    if (isInside(mousePos, signUpButtons[0])) {
        usernameFieldSignup.value = '';
        usernameFieldSignup.style.display = 'none';
        emailFieldSignUp.value = '';
        emailFieldSignUp.style.display = 'none';
        passwordFieldSignUp.value = '';
        passwordFieldSignUp.style.display = 'none';
        passwordConfirmFieldSignUp.value = '';
        passwordConfirmFieldSignUp.style.display = 'none';
        drawMenu(loginButtons, 0);
    } else if (isInside(mousePos, signUpButtons[1])) {
        let emailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (usernameFieldSignup.value == '' || emailFieldSignUp.value == '' || passwordFieldSignUp.value == '' || passwordConfirmFieldSignUp.value == '') {
            alert('One or more of the required fields are empty.');
        } else if (passwordFieldSignUp.value != passwordConfirmFieldSignUp.value) {
            alert('Passwords do not match');
        } else if (passwordFieldSignUp.value.length < 8) {
            alert('Passwords must be atleast 8 Characters');
        } else if (!emailRegex.test(emailFieldSignUp.value)) {
            alert('Invalid Email Address');
        } else {
            sock.emit('SignUpAttempt', { username: usernameFieldSignup.value, email: emailFieldSignUp.value, password: passwordFieldSignUp.value, passwordConfirm: passwordConfirmFieldSignUp.value });
        }
    }
}

function forgotPasswordListener(evt) {
    let mousePos = getMousePos(menu_canvas, evt);
    if (isInside(mousePos, forgotPasswordButtons[0])) {
        forgotPasswordUsernameField.value = '';
        forgotPasswordUsernameField.style.display = 'none';
        forgotPasswordEmailField.value = '';
        forgotPasswordEmailField.style.display = 'none';
        drawMenu(loginButtons, 0);
    } else if (isInside(mousePos, forgotPasswordButtons[1])) {
        sock.emit('passwordRecovery', {userID:forgotPasswordUsernameField.value, email:forgotPasswordEmailField.value})
    }
}

function mainListener(evt) {
    let mousePos = getMousePos(menu_canvas, evt);
    if (isInside(mousePos, mainMenuButtons[0])) {
        drawMenu(playMenuButtons, 3);
    } else if (isInside(mousePos, mainMenuButtons[1])) {
        drawMenu(createMenuButtons, 7);
    } else if (isInside(mousePos, mainMenuButtons[2])) {
        settingsButtons[3].isEnabled = settings.musicOn;
        settingsButtons[5].isEnabled = settings.soundOn;
        drawMenu(settingsButtons, 9);
    } else if (isInside(mousePos, mainMenuButtons[3])) {
        userID = null;
        settings.musicOn = false;
        settings.soundOn = false;
        resetCampaignProgress();
        drawMenu(loginButtons, 0);
    }
}

function playListener(evt) {
    let mousePos = getMousePos(menu_canvas, evt);
    if (isInside(mousePos, playMenuButtons[0])) {
        drawMenu(campaignButtons, 4);
    } else if (isInside(mousePos, playMenuButtons[1])) {
        drawMenu(searchedLevelButtons, 5);
    } else if (isInside(mousePos, playMenuButtons[2])) {
        sock.emit('retrieveRandomLevel', {});
    } else if (isInside(mousePos, playMenuButtons[3])) {
        drawMenu(mainMenuButtons, 2);
    }
}

function createListener(evt) {
    let mousePos = getMousePos(menu_canvas, evt);
    if (isInside(mousePos, createMenuButtons[0])) {
        document.getElementById('menuDiv').style.display = 'none';
        document.getElementById('levelEditorDiv').style.display = 'block';
        levelEditorLevelID = null;
    } else if (isInside(mousePos, createMenuButtons[1])) {
        sock.emit('playerLevelRequest', { userID: userID });
    } else if (isInside(mousePos, createMenuButtons[2])) {
        drawMenu(mainMenuButtons, 2);
    }
}

function settingsListener(evt) {
    let mousePos = getMousePos(menu_canvas, evt);
    if (isInside(mousePos, settingsButtons[0])) { //Back button
        settings.keyBinds = [settingsButtons[7].currentKey, settingsButtons[9].currentKey, settingsButtons[11].currentKey, settingsButtons[13].currentKey, settingsButtons[15].currentKey, settingsButtons[17].currentKey, settingsButtons[19].currentKey, settingsButtons[21].currentKey];
        for (i = 0 ; i < settingsButtons.length ; i++) {
            if (settingsButtons[i].isListening) {
                settingsButtons[i].isListening = false;
            }
        }
        window.removeEventListener('keydown', ListenForKeyBinds);
        socket.emit('settingsUpdate', { userID: userID, settings:settings }); //This doesnt need a response, it just updates the database.
        drawMenu(mainMenuButtons, 2);
    } else if (isInside(mousePos, settingsButtons[1])) { //Button to send user to their profile page
        drawMenu(playerProfileButtons, 11);
    } else if (isInside(mousePos, settingsButtons[3])) { //Button for music on/off
        settingsButtons[3].isEnabled = !settingsButtons[3].isEnabled;
        settings.musicOn = settingsButtons[3].isEnabled;
        //call whatever will turn on/off music
        drawMenu(settingsButtons, 9)
    } else if (isInside(mousePos, settingsButtons[5])) { //Button for sound on/off
        settingsButtons[5].isEnabled = !settingsButtons[5].isEnabled;
        settings.soundOn = settingsButtons[5].isEnabled;
        //call whatever will turn on/off sound fx
        drawMenu(settingsButtons, 9)
    } else if (isInside(mousePos, settingsButtons[7])) { //Button for Left Keybind
        settingsButtons[7].isListening = true;
        window.addEventListener('keydown', ListenForKeyBinds);
        drawMenu(settingsButtons, 9)
    }else if (isInside(mousePos, settingsButtons[9])) { //Button for Right Keybind
        settingsButtons[9].isListening = true;
        window.addEventListener('keydown', ListenForKeyBinds);
        drawMenu(settingsButtons, 9)
    }else if (isInside(mousePos, settingsButtons[11])) { //Button for Jump Keybind
        settingsButtons[11].isListening = true;
        window.addEventListener('keydown', ListenForKeyBinds);
        drawMenu(settingsButtons, 9)
    }else if (isInside(mousePos, settingsButtons[13])) { //Button for Down Keybind
        settingsButtons[13].isListening = true;
        window.addEventListener('keydown', ListenForKeyBinds);
        drawMenu(settingsButtons, 9)
    }else if (isInside(mousePos, settingsButtons[15])) { //Button for Attack Keybind
        settingsButtons[15].isListening = true;
        window.addEventListener('keydown', ListenForKeyBinds);
        drawMenu(settingsButtons, 9)
    }else if (isInside(mousePos, settingsButtons[17])) { //Button for Action Keybind
        settingsButtons[17].isListening = true;
        window.addEventListener('keydown', ListenForKeyBinds);
        drawMenu(settingsButtons, 9)
    }else if (isInside(mousePos, settingsButtons[19])) { //Button for weaponToggle Keybind
        settingsButtons[19].isListening = true;
        window.addEventListener('keydown', ListenForKeyBinds);
        drawMenu(settingsButtons, 9)
    }else if (isInside(mousePos, settingsButtons[21])) { //Button for Pause Keybind
        settingsButtons[21].isListening = true;
        window.addEventListener('keydown', ListenForKeyBinds);
        drawMenu(settingsButtons, 9)
    }
}

function ListenForKeyBinds(evt) {
    for (i = 5 ; i < settingsButtons.length ; i++) {
        if (settingsButtons[i].isListening === true) {
            settingsButtons[i].currentKey = evt.keyCode;
            settingsButtons[i].text = String.fromCharCode(evt.keyCode)
            settingsButtons[i].isListening = false;
        }
    }
    drawMenu(settingsButtons, 9);
    window.removeEventListener('keydown', ListenForKeyBinds);
}

function playerProfileListener(evt) {
    let mousePos = getMousePos(menu_canvas, evt);
    if (isInside(mousePos, settingsButtons[0])) { //Back button
        drawMenu(settingsButtons, 9);
    } else if (isInside(mousePos, settingsButtons[1])) { //Button to send user to their profile page
        drawMenu(changePasswordButtons, 12);
    }
}

function changePasswordListener(evt) {
    let mousePos = getMousePos(menu_canvas, evt);
    if (isInside(mousePos, changePasswordButtons[0])) { //Back button
        changePasswordField.style.display = 'none';
        changePasswordConfirmField.style.display = 'none';
        changePasswordField.value = '';
        changePasswordConfirmField.value = '';
        drawMenu(playerProfileButtons, 11);
    } else if (isInside(mousePos, changePasswordButtons[1])) { //Button to send user to their profile page
        if (changePasswordConfirmField.value !== changePasswordField.value) {
            alert('Password fields do not match');
        } else if (changePasswordField.value.length < 8) {
            alert('Password must be atleast 8 characters')
        } else {
            sock.emit('passwordUpdate', {userID:userID, newPassword:changePasswordField.value});
        }
    }
}

function levelPageListener(evt) {
    let mousePos = getMousePos(menu_canvas, evt);
    if (isInside(mousePos, levelPageButtons[0])) { //Back Button
        levelPageButtons[1].text = '';
        levelPageButtons[2].text = '';
        levelPageButtons[3].text = '';
        levelPageButtons[3].levelID = '';
        levelPageButtons[5].text = '';
        if (previousMenuState === 3) {
            drawMenu(playMenuButtons, 3);
        } else if (previousMenuState === 5) {
            drawMenu(searchedLevelButtons, 5);
        } else if (previousMenuState === 8) {
            drawMenu(editLevelButtons, 8);
        }
    } else if (isInside(mousePos, levelPageButtons[4])) { //Play button
        document.getElementById('menuDiv').style.display = 'none';
        myGameArea.gameCanvas.style.display = 'block';
        gameEngineLevelID = levelPageButtons[3].levelID;
        startGame(levelPageButtons[4].tileArray);
    }
}

function searchLevelMenuListener(evt) {
    let mousePos = getMousePos(menu_canvas, evt);
    if (isInside(mousePos, searchedLevelButtons[0])) { //Back Button
        resetLevelList(searchedLevelButtons);
        levelSearchField.style.display = 'none';
        levelSearchField.value = '';
        drawMenu(playMenuButtons, 3);
        levelListPageIndex = 0;
    } else if (isInside(mousePos, searchedLevelButtons[1])) { //Search levelButtonList
        if (levelSearchField.value != "") {
            sock.emit('searchLevel', { searchFieldData: levelSearchField.value });
            levelListPageIndex = 0;
            populateSearchList();
            drawMenu(searchedLevelButtons, 5);
        } else {
            alert('Empty field');
        }
    } else if (isInside(mousePos, searchedLevelButtons[2])) { //Level 1 Button, redirects to Level Page
        if (searchedLevelButtons[2].containsLevel == true) {
            levelSearchField.style.display = 'none';
            sock.emit('loadLevel', { levelID: searchedLevelButtons[2].levelID });
        }
    } else if (isInside(mousePos, searchedLevelButtons[3])) { //Level 2 Button, redirects to Level Page
        if (searchedLevelButtons[3].containsLevel == true) {
            levelSearchField.style.display = 'none';
            sock.emit('loadLevel', { levelID: searchedLevelButtons[3].levelID });
        }
    } else if (isInside(mousePos, searchedLevelButtons[4])) { //Level 3 Button, redirects to Level Page
        if (searchedLevelButtons[4].containsLevel == true) {
            levelSearchField.style.display = 'none';
            sock.emit('loadLevel', { levelID: searchedLevelButtons[4].levelID });
        }
    } else if (isInside(mousePos, searchedLevelButtons[5])) { //Level 4 Button, redirects to Level Page
        if (searchedLevelButtons[5].containsLevel == true) {
            levelSearchField.style.display = 'none';
            sock.emit('loadLevel', { levelID: searchedLevelButtons[5].levelID });
        }
    } else if (isInside(mousePos, searchedLevelButtons[6])) { //Level 5 Button, redirects to Level Page
        if (searchedLevelButtons[6].containsLevel == true) {
            sock.emit('loadLevel', { levelID: searchedLevelButtons[6].levelID });
            levelSearchField.style.display = 'none';
        }
    } else if (isInside(mousePos, searchedLevelButtons[7])) { //Previous page in list button
        if (levelListPageIndex != 0) {
            levelListPageIndex = levelListPageIndex - 1;
            populateSearchList();
            drawMenu(searchedLevelButtons, 5);
        } else {
            alert('First Page');
        }
    } else if (isInside(mousePos, searchedLevelButtons[8])) { //Next page in list button
        if (levelListPageIndex != (Math.ceil(levelList.length / 5 - 1))) {
            levelListPageIndex = levelListPageIndex + 1;
            populateSearchList();
            drawMenu(searchedLevelButtons, 5);
        } else {
            alert('Last Page');
        }
    }
}

function editLevelMenuListener(evt) {
    let mousePos = getMousePos(menu_canvas, evt);
    if (isInside(mousePos, editLevelButtons[0])) {
        resetLevelList(editLevelButtons);
        drawMenu(createMenuButtons, 7);
    } else if (isInside(mousePos, editLevelButtons[1])) {
        if (editLevelButtons[1].containsLevel) {
            previousMenuState = 404;
            sock.emit('loadLevel', { levelID: editLevelButtons[1].levelID });
        }
    } else if (isInside(mousePos, editLevelButtons[2])) {
        if (editLevelButtons[1].containsLevel) {
            previousMenuState = 808;
            sock.emit('loadLevel', { levelID: editLevelButtons[1].levelID });
        }
    } else if (isInside(mousePos, editLevelButtons[3])) {
        if (editLevelButtons[1].containsLevel) {
            sock.emit('deleteLevel', { levelID: editLevelButtons[1].levelID, userID:userID });
        }
    } else if (isInside(mousePos, editLevelButtons[4])) {
        if (editLevelButtons[4].containsLevel) {
            previousMenuState = 404;
            sock.emit('loadLevel', { levelID: editLevelButtons[4].levelID });
        }
    } else if (isInside(mousePos, editLevelButtons[5])) {
        if (editLevelButtons[4].containsLevel) {
            previousMenuState = 808;
            sock.emit('loadLevel', { levelID: editLevelButtons[4].levelID });
        }
    } else if (isInside(mousePos, editLevelButtons[6])) {
        if (editLevelButtons[4].containsLevel) {
            sock.emit('deleteLevel', { levelID: editLevelButtons[4].levelID, userID:userID });
        }
    } else if (isInside(mousePos, editLevelButtons[7])) {
        if (editLevelButtons[7].containsLevel) {
            previousMenuState = 404;
            sock.emit('loadLevel', { levelID: editLevelButtons[7].levelID });
        }
    } else if (isInside(mousePos, editLevelButtons[8])) {
        if (editLevelButtons[7].containsLevel) {
            previousMenuState = 808;
            sock.emit('loadLevel', { levelID: editLevelButtons[7].levelID });
        }
    } else if (isInside(mousePos, editLevelButtons[9])) {
        if (editLevelButtons[7].containsLevel) {
            sock.emit('deleteLevel', { levelID: editLevelButtons[7].levelID, userID:userID });
        }
    } else if (isInside(mousePos, editLevelButtons[10])) {
        if (editLevelButtons[10].containsLevel) {
            previousMenuState = 404;
            sock.emit('loadLevel', { levelID: editLevelButtons[10].levelID });
        }
    } else if (isInside(mousePos, editLevelButtons[11])) {
        if (editLevelButtons[10].containsLevel) {
            previousMenuState = 808;
            sock.emit('loadLevel', { levelID: editLevelButtons[10].levelID });
        }
    } else if (isInside(mousePos, editLevelButtons[12])) {
        if (editLevelButtons[10].containsLevel) {
            sock.emit('deleteLevel', { levelID: editLevelButtons[10].levelID, userID:userID });
        }
    } else if (isInside(mousePos, editLevelButtons[13])) {
        if (editLevelButtons[13].containsLevel) {
            previousMenuState = 404;
            sock.emit('loadLevel', { levelID: editLevelButtons[13].levelID });
        }
    } else if (isInside(mousePos, editLevelButtons[14])) {
        if (editLevelButtons[13].containsLevel) {
            previousMenuState = 808
            sock.emit('loadLevel', { levelID: editLevelButtons[13].levelID });
        }
    } else if (isInside(mousePos, editLevelButtons[15])) {
        if (editLevelButtons[13].containsLevel) {
            sock.emit('deleteLevel', { levelID: editLevelButtons[13].levelID, userID:userID });
        }
    } else if (isInside(mousePos, editLevelButtons[16])) {
        if (levelListPageIndex != 0) {
            levelListPageIndex = levelListPageIndex - 1;
            populateEditList();
            drawMenu(editLevelButtons, 8);
        } else {
            alert('This is the first page');
        }
    } else if (isInside(mousePos, editLevelButtons[17])) {
        if (levelListPageIndex != (Math.ceil(levelList.length / 5) - 1)) {
            levelListPageIndex = levelListPageIndex + 1;
            populateEditList()
            drawMenu(editLevelButtons, 8);
        } else {
            alert('This is the last page');
        }
    }
}

function campaignListener(evt) {
    let mousePos = getMousePos(menu_canvas, evt);
    if (isInside(mousePos, campaignButtons[0])) {
        drawMenu(playMenuButtons, 3);
    } else if (isInside(mousePos, campaignButtons[1])) {
        if (campaignButtons[1].isUnlocked) {
            sock.emit('loadLevel', { levelID: campaignButtons[1].levelID })
        } else {
            //Play a sound effect like a sort of error message to let the user know they cant play it.
        }
    } else if (isInside(mousePos, campaignButtons[2])) {
        if (campaignButtons[2].isUnlocked) {
            sock.emit('loadLevel', { levelID: campaignButtons[2].levelID })
        } else {
            //Play a sound effect like a sort of error message to let the user know they cant play it.
        }
    } else if (isInside(mousePos, campaignButtons[3])) {
        if (campaignButtons[3].isUnlocked) {
            sock.emit('loadLevel', { levelID: campaignButtons[3].levelID })
        } else {
            //Play a sound effect like a sort of error message to let the user know they cant play it.
        }
    } else if (isInside(mousePos, campaignButtons[4])) {
        if (campaignButtons[4].isUnlocked) {
            sock.emit('loadLevel', { levelID: campaignButtons[4].levelID })
        } else {
            //Play a sound effect like a sort of error message to let the user know they cant play it.
        }
    } else if (isInside(mousePos, campaignButtons[5])) {
        if (campaignButtons[5].isUnlocked) {
            sock.emit('loadLevel', { levelID: campaignButtons[5].levelID })
        } else {
            //Play a sound effect like a sort of error message to let the user know they cant play it.
        }
    } else if (isInside(mousePos, campaignButtons[6])) {
        if (campaignButtons[6].isUnlocked) {
            sock.emit('loadLevel', { levelID: campaignButtons[6].levelID })
        } else {
            //Play a sound effect like a sort of error message to let the user know they cant play it.
        }
    } else if (isInside(mousePos, campaignButtons[7])) {
        if (campaignButtons[7].isUnlocked) {
            sock.emit('loadLevel', { levelID: campaignButtons[7].levelID })
        } else {
            //Play a sound effect like a sort of error message to let the user know they cant play it.
        }
    } else if (isInside(mousePos, campaignButtons[8])) {
        if (campaignButtons[8].isUnlocked) {
            sock.emit('loadLevel', { levelID: campaignButtons[8].levelID })
        } else {
            //Play a sound effect like a sort of error message to let the user know they cant play it.
        }
    } else if (isInside(mousePos, campaignButtons[9])) {
        if (campaignButtons[9].isUnlocked) {
            sock.emit('loadLevel', { levelID: campaignButtons[9].levelID })
        } else {
            //Play a sound effect like a sort of error message to let the user know they cant play it.
        }
    } else if (isInside(mousePos, campaignButtons[10])) {
        if (campaignButtons[10].isUnlocked) {
            sock.emit('loadLevel', { levelID: campaignButtons[10].levelID })
        } else {
            //Play a sound effect like a sort of error message to let the user know they cant play it.
        }
    }
}

//SOCKET RESPONSES

//Response to the user clicking login on login page
sock.on('loginVerif', function (data) {
    if (data.success) {
        usernameFieldLogin.value = '';
        usernameFieldLogin.style.display = 'none';
        passwordFieldLogin.value = '';
        passwordFieldLogin.style.display = 'none';
        userID = data.user.userID;
        storyProgress = data.user.saveState;
        setCampaignProgress();
        settings.musicOn = data.user.settings.musicOn;
        settings.soundOn = data.user.settings.soundOn;
        settings.keyBinds = data.user.settings.keyBinds;
        drawMenu(mainMenuButtons, 2);
    } else {
        alert(data.err);
    }
});

///Response to the user clicking sign up on sign up page
sock.on('SignUpVerif', function (data) {
    if (data.success) {
        drawMenu(mainMenuButtons, 2);
        userID = data.user.userID;
        setCampaignProgress();
        usernameFieldSignup.value = '';
        usernameFieldSignup.style.display = 'none';
        emailFieldSignUp.value = '';
        emailFieldSignUp.style.display = 'none';
        passwordFieldSignUp.value = '';
        passwordFieldSignUp.style.display = 'none';
        passwordConfirmFieldSignUp.value = '';
        passwordConfirmFieldSignUp.style.display = 'none';
    } else {
        alert(data.err);
    }
});

sock.on('passwordRecoveryResponse', function(data) {
    if (data.success) {
        alert('Email sent!');
    } else {
        alert(data.err);
    }
});

sock.on('passwordUpdateResponse', function(data){
    if (data.success) {
        alert('Password Changed');
    } else {
        alert(data.err);
    }
});

sock.on('saveCampaignResponse', function(data){
    if (data.success) {
        setCampaignProgress();
        drawMenu(campaignButtons, 4);
    } else {
        alert(data.err)
    }
})

//Response to user clicking 'Random Level' On Play Menu or user clicking on a level in the Search Menu
//Response to user clicking on a Level in 'Edit Level' menu.
//Response to user clicking on level from campaign
//Response to user clicking on 'Play' in Level Page
sock.on('retrieveLevelResponse', function (data) {
    if (data.success) {
        if (currentMenuState == 5 || currentMenuState == 3) { //Random Level was clicked or A searched level was clicked
            levelPageButtons[3].text = data.levelName;
            levelPageButtons[3].levelID = data.levelID;
            levelPageButtons[1].text = data.userID;
            levelPageButtons[5].text = data.levelDesc;
            levelPageButtons[4].tileArray = data.tileArray;
            //levelPageButtons[2] = data.thumbnail;
            sock.emit('loadHighscores', { levelID:levelPageButtons[3].levelID });
            drawMenu(levelPageButtons, 6);
        } else if (currentMenuState == 8) { //Current Menu == Edit Level Page
            if (previousMenuState === 404) { //User clicked on Level from the Edit Level Page
                levelPageButtons[3].text = data.levelName;
                levelPageButtons[3].levelID = data.levelID;
                levelPageButtons[1].text = data.userID;
                levelPageButtons[5].text = data.levelDesc;
                levelPageButtons[4].tileArray = data.tileArray;
                //levelPageButtons[2] = data.thumbnail;
                drawMenu(levelPageButtons, 6);
            } else if (previousMenuState === 808) { //User clicked on 'Edit' for a level in edit level page
                previousMenuState = 7;
                document.getElementById('menuDiv').style.display = 'none';
                document.getElementById('levelEditorDiv').style.display = 'block';
                levelEditorLevelID = data.levelID;
                loadLevel(data.tileArray);
            }
        } else if (currentMenuState == 4) { //User clicked on Campaign Level
            document.getElementById('menuDiv').style.display = 'none';
            myGameArea.gameCanvas.style.display = 'block';
            gameEngineLevelID = data.levelID;
            startGame(data.tileArray);
        }
    } else {
        alert(data.err);
    }
});

//Response to the user clicking 'Edit Level' on Create Menu.
//Response to the user clicking 'Search' on Search Level Menu
sock.on('playerLevelResponse', function (data) {
    if (data.success) {
        levelList = data.levelList;
        if (currentMenuState == 5) {
            populateSearchList();
            drawMenu(searchedLevelButtons, 5);
        } else if (currentMenuState == 7) {
            populateEditList();
            drawMenu(editLevelButtons, 8);
        }
    } else {
        alert(data.err)
    }
});

//Response to Client requesting leaderboard on Level Page
sock.on('loadHighscoresResponse', function (data) {
    if (data.success) {
        for (i = 0; i < 10; i++) {
            if (data.highscore[i] !== undefined) {
                let leaderboardRankRect = {
                    x: 740,
                    y: 20 + (68 * i),
                    height: 68,
                    width: 40,
                    text: i + 1
                }
                CreateButton(leaderboardRankRect);
                let leaderboardUserRect = {
                    x: 780,
                    y: 20 + (68 * i),
                    height: 68,
                    width: 300,
                    text: data.highscore[i].userID,
                    font: '25pt Impact'
                }
                CreateButton(leaderboardUserRect);
                let leaderboardMinutes = Math.floor(data.highscore[i].time / 60);
                let leaderboardSeconds = (data.highscore[i].time % 60);
                if (leaderboardSeconds < 10) {
                    leaderboardSeconds = '0'+leaderboardSeconds
                }
                let leaderboardTime = leaderboardMinutes + ':' + leaderboardSeconds;
                let leaderboardTimeRect = {
                    x: 1080,
                    y: 20 + (68 * i),
                    height: 68,
                    width: 150,
                    text: leaderboardTime,
                    font: '25pt Impact'
                }
                CreateButton(leaderboardTimeRect);
            }
        }
    } else {
        alert(data.err);
        let leaderboardRankRect = {
            x: 725,
            y: 20 + (68 * i),
            height: 68,
            width: 55,
            text: i + 1
        }
        CreateButton(leaderboardRankRect);
        let leaderboardUserRect = {
            x: 780,
            y: 20 + (68 * i),
            height: 68,
            width: 300,
            text: '',
            font: '25pt Impact'
        }
        CreateButton(leaderboardUserRect);
        let leaderboardTimeRect = {
            x: 1080,
            y: 20 + (68 * i),
            height: 68,
            width: 150,
            text: '',
            font: '25pt Impact'
        }
        CreateButton(leaderboardTimeRect);
    }
});

//UTILITY FUNCTIONS
//Resets all values inside the level list variable which is used for Player's Created levels and Searched levels.
function resetLevelList(levelButtonList) {
    levelListPageIndex = 0;
    levelList = null;
    for (i = 0; i < levelButtonList.length; i++) {
        if (levelButtonList[i].containsLevel === true) {
            levelButtonList[i].containsLevel = false;
            if (levelButtonList[i].levelID !== undefined) {
                levelButtonList[i].text = '';
                levelButtonList[i].levelID = '';
            }
        }
    }
}

//Fills the Player's created levels based off data from database.
function populateEditList() {
    for (i = 0; i < 5; i++) {
        if ((levelList[i + (levelListPageIndex * 5)]) !== undefined) {
            editLevelButtons[(3 * i) + 1].text = levelList[i + (levelListPageIndex * 5)].levelName;
            editLevelButtons[(3 * i) + 1].levelID = levelList[i + (levelListPageIndex * 5)].levelID;
            editLevelButtons[(3 * i) + 1].containsLevel = true;
            editLevelButtons[(3 * i) + 2].containsLevel = true;
            editLevelButtons[(3 * i) + 3].containsLevel = true;
        } else {
            editLevelButtons[(3 * i) + 1].text = '';
            editLevelButtons[(3 * i) + 1].levelID = '';
            editLevelButtons[(3 * i) + 1].containsLevel = false;
            editLevelButtons[(3 * i) + 2].containsLevel = false;
            editLevelButtons[(3 * i) + 3].containsLevel = false;
        }
    }
}

//Fills the Searched levels based off data from database.
function populateSearchList() {
    for (i = 0; i < 5; i++) {
        if (levelList[(i + (levelListPageIndex * 5))] !== undefined) {
        //This line is throwing an error every search but I cant figure out why???
        //It doesnt break anything, it just gives an error and breaks at the end of searching
        //which is fine, theres nothing else to load, but still want to fix
            searchedLevelButtons[i + 2].text = levelList[i + (levelListPageIndex * 5)].levelName;
            searchedLevelButtons[i + 2].levelID = levelList[i + (levelListPageIndex * 5)].levelID;
            searchedLevelButtons[i + 2].containsLevel = true;
        } else {
            searchedLevelButtons[i + 2].text = '';
            searchedLevelButtons[i + 2].levelID = '';
            searchedLevelButtons[i + 2].containsLevel = false;
        }
    }
}

function resetCampaignProgress() {
    storyProgress = null;
    for (i = 1; i <= 10; i++){
        campaignButtons[i].isCompleted = false;
        campaignButtons[i].isUnlocked = false;
    }
    campaignButtons[1].isUnlocked = true;
}
function setCampaignProgress() {
    for (i = 0; i <= storyProgress; i++) {
        if (i != 0) {
            campaignButtons[i].isCompleted = true;
        }
        if (i > 9)return;//array out of bounds otherwise
        campaignButtons[i+1].isUnlocked = true;
    }
}

menuBackgroundJS.onload = function () {
    menu_ctx.drawImage(menuBackgroundJS, 0, 0, canvasWidth, canvasHeight);
    drawMenu(loginButtons, 0);
}
menuBackgroundJS.src = './client/assets/menuBackground.jpg';