"use strict";
let gameStartedFrom = 0;
let MAXSPEEDX = 4;
let MAXSPEEDY = 9;
let milli = 0;
let seconds = 0;
let minutes = 0;
let damageMilli = 0;
let damageSeconds = 0;
let invincibiltyFrames;
let timer;
let gameEngineLevelID = 0;
let Player;
let gravity = .41;
let godMode;
let startHP = 100;
let backgroundImage = new Image();
let entityArray = [];
let entityArray2 = [];
let entitiesToDestroy = [];
let idRegister = 0;
let tookDamage;
let didDamage;
let playerHPIsDepleted = false;
let keyBinds = [65, 68, 87, 83, 32, 69, 81, 27];//left, right, jump, down, attack, action, weapon, pause 

let pauseMenuButtons = [{ x: 300, y: 190, width: 680, height: 340, text: '' }, { x: 350, y: 260, width: 280, height: 200, text: 'Exit To Menu', wrap: true }, { x: 652, y: 260, width: 280, height: 200, text: 'Unpause' }];
let endCampaignButtons = [{ x: 300, y: 190, width: 680, height: 340, text: '' }, { x: 350, y: 260, width: 280, height: 200, text: 'Exit To Menu', wrap: true }, { x: 652, y: 260, width: 280, height: 200, text: 'Replay Level' }];

// Loading sound effects 
// Due to Chrome's policy on auto-playing sounds you will need to change the option in " chrome://flags/#autoplay-policy "
let backgroundSound;
let DyingSound;
let HitSound = new sound("./client/assets/soundeffects/enemyHit.mp3");
let levelFinishSound = new sound("./client/assets/soundeffects/levelcomplete.mp3");
let jumpSound = new sound("./client/assets/soundeffects/jumpsound.wav");

let assignNewID = function () {
    idRegister++;
    return idRegister;
}

class Entity {

    constructor(type) {
        this.id = assignNewID();
        this.type = type;
    }

}

class Component {

}

class CTransform extends Component {
    constructor(x, y, xSpeed, ySpeed) {
        super();
        this.x = x;
        this.y = y;
        this.prevX = x;
        this.prevY = y;
        this.xSpeed = xSpeed;
        this.ySpeed = ySpeed;
    }

}

class CGravity extends Component {	//just a flag for physics reference
    constructor() {
        super();
    }
}

class CBoundingBox extends Component {
    constructor(width, height) {
        super();
        this.width = width;
        this.height = height;
    }
}

class CCollectible extends Component {
    constructor() {
        super();

    }
}

class CInteractable extends Component {
    constructor(action) {   //this 'action' param should be a function.
        super();
        this.action = action;

        /*
         *  It's worth noting here that the 'action' param will be, for an entity with
         *  CVisibilitySwitch for example, a toggle of isVisible for all entities with 
         *  CVisibilityToggle.
         *  
         *  This component will attach to anything that the player interacts with by way of the
         *  'use' key. The possibilities are boundless thanks to JavaScript's handling of functions
         *  as variables.
         */

    }
}

class CState extends Component {
    constructor(state) {
        super();
        this.state = state;
        this.prevState = state;
        this.stateChanged = false;
    }
}

// Probably don't need this; just create a trap entity 
class CTrap extends Component {
    constructor() {
        super();
        //this.damage = damage;  ?
    }

}

class CVisibilitySwitch extends Component {
    constructor() {
        super();
    }
}

class CVisibilityToggle extends Component {
    constructor(initiallyVisible) {
        super();
        this.isVisible = initiallyVisible;
    }
}

class CHealth extends Component {
    constructor(HP) {		//it likely doesn't matter that this component always constructs with currentHP maxed out
        super();
        this.maxHP = HP;
        this.currentHP = HP;
    }
}

class CFollow extends Component {
    constructor(speed, home) {
        super();
        this.speed = speed;
        this.home = home;
    }
}

class CPatrol extends Component {
    constructor(positions, speed) {
        super();
        this.positions = positions;
        this.speed = speed;
    }
}

class CScore extends Component {
    constructor(score) {
        super();
        this.score = score;
    }
}

class PlayerInput {
    constructor() {
        //super();
        this.upInput = false;
        this.leftInput = false;
        this.rightInput = false;
        this.downInput = false;     //SSBM-style fastfall wouldn't be amiss here. Tight movement will be a theme.
        this.weaponToggleInput = false;
        this.actionInput = false;
        this.attackInput = false;
    }
}

class Weapon extends Component {
    constructor(weapon) {
        super();
        this.weapon = weapon;
    }
}

class CMultipleAnimationSet extends Component {
    constructor(animations) {
        super();
        this.animations = animations;
    }
}

class Animation extends Component {
    constructor(frameWidth, frameHeight, frameCount, imageSrc) {
        super();
        this.frameWidth = frameWidth;
        this.frameHeight = frameHeight;
        this.frameCount = frameCount;
        this.image = new Image();
        this.image.width = frameWidth / 2;
        this.image.height = frameHeight / 2;
        this.imageSrc = imageSrc;
        this.image.src = this.imageSrc;
        this.currentFrame = 0;

        this.update = function () {

            if (this.frameCount < 2) {
                return;
            }

            this.currentFrame += 1;

            if (this.currentFrame >= frameCount) {
                this.currentFrame = 0;
            }

        }

    }

}

class CAnimation extends Component {
    constructor(animation, repeat) {
        super();
        this.animation = animation;
        this.repeat = repeat;
    }

}

let input = new PlayerInput();

function spawnPlayer(posx, posy) {
    Player = new Entity("Player");
    Player.cBoundingBox = new CBoundingBox(32, 32);
    Player.cTransform = new CTransform(posx, posy, 1, 1);
    Player.cGravity = new CGravity();
    Player.cState = new CState("standing");
    Player.cHealth = new CHealth(100);
    Player.cScore = new CScore(0);

    let a0 = "./client/assets/player.png";         //64, 64, 1
    let a1 = "./client/assets/playerWalkLeft.png";      //64, 64, 9
    let a2 = "./client/assets/playerWalkRight.png";     //64, 64, 9
    let a3 = "./client/assets/playerThrustLeft.png";    //64, 64, 8
    let a4 = "./client/assets/playerThrustRight.png";   //64, 64, 8
    let animSet = [];
    animSet.push(a0, a1, a2, a3, a4);
    Player.cAnimation = new CAnimation(new Animation(64, 64, 1, a0), true);
    let a = new CMultipleAnimationSet(animSet)
    Player.cMultipleAnimationSet = a;
    entityArray.push(Player);
    console.log("Player spawned at: " + posx + " " + posy);
    playerHPIsDepleted = false;
}

//+viewport
let viewport = {
    screen: [0, 0],
    offset: [0, 0],
    start: [0, 0],
    end: [0, 0],
    update: function (px, py) {
        this.offset[0] = Math.floor((this.screen[0] / 2) - px);
        this.offset[1] = Math.floor((this.screen[1] / 2) - py);
        this.start[0] = px - Math.ceil(this.screen[0] / 2);
        this.start[1] = py - Math.ceil(this.screen[1] / 2);

        if (this.start[0] < 0) { this.start[0] = 0 }
        if (this.start[1] < 0) { this.start[1] = 0 }

        this.end[0] = this.start[0] + 1280;
        this.end[1] = this.start[1] + 720;

        if (this.end[0] > 3840) { this.end[0] = 3840 }
        if (this.end[1] > 720) { this.end[1] = 720 }
    }
};
//-viewport

let startTimer = function () {
    milli += 35;
    if (milli >= 1000) {
        milli = 0;
        seconds++;
        if (seconds == 60) {
            seconds = 0;
            minutes++;
        }
    }
}
// Counter for player taking damage 
let damageFrames = function () {
    damageMilli += 35;
    if (damageMilli >= 1000) {
        damageMilli = 0;
        damageSeconds++;
        if (damageSeconds == 2) {
            damageSeconds = 0;
            tookDamage = false;
        }
    }
}


function startGame(tileArray) {
    // Clear any intervals each time the game starts 
    clearInterval(timer);
    clearInterval(invincibiltyFrames);
    if (myGameArea.interval) { clearInterval(myGameArea.interval); }
    window.addEventListener('keydown', keysDown);
    window.addEventListener('keyup', keysUp);
    godMode = false;
    keyBinds = settings.keyBinds;
    entityArray2 = tileArray;
    entityArray = [];
    minutes = 0;
    seconds = 0;
    milli = 0;
    // timer will start when the level is started 
    timer = setInterval(startTimer, 35);
    invincibiltyFrames = setInterval(damageFrames, 35);
    
    for (let i = 0; i < tileArray.length; i++) {
        let entity;
        switch (tileArray[i].ptype) {

            case "w":
                entity = new Entity("Tile");
                entity.cBoundingBox = new CBoundingBox(32, 32);
                entity.cAnimation = new CAnimation(new Animation(64, 64, 1, "./client/assets/wall.png"), true);
                entity.cTransform = new CTransform(tileArray[i].px, tileArray[i].py, 0, 0);
                entityArray.push(entity);
                break;
            case "s":
                entity = new Entity("Tile");
                entity.cBoundingBox = new CBoundingBox(32, 32);
                entity.cAnimation = new CAnimation(new Animation(96, 96, 1, "./client/assets/stone.jpg"), true);
                entity.cTransform = new CTransform(tileArray[i].px, tileArray[i].py, 0, 0);
                entityArray.push(entity);
                break;
            case "b":
                backgroundImage.src = "./client/assets/" + tileArray[i].pname;
                break;
            case "e":
                let testNPC = new Entity("NPC");
                testNPC.cBoundingBox = new CBoundingBox(32, 32);
                let a1 = "./client/assets/enemyMeleeWalkLeft.png";    //64, 64, 9
                let a2 = "./client/assets/enemyMeleeWalkRight.png";   //64, 64, 9
                let animSet = [];
                animSet.push(a1, a2);
                let a = new CMultipleAnimationSet(animSet);
                testNPC.cMultipleAnimationSet = a;
                testNPC.cAnimation = new CAnimation(new Animation(64, 64, 9, testNPC.cMultipleAnimationSet.animations[0]), true);
                testNPC.cTransform = new CTransform(tileArray[i].px, tileArray[i].py, tileArray[i].px, tileArray[i].py, 0, 0);
                testNPC.cState = new CState("standing");
                testNPC.cHealth = new CHealth(100);
                testNPC.cFollow = new CFollow(1, tileArray[i].px, tileArray[i].py);
                testNPC.cGravity = new CGravity();
                entityArray.push(testNPC);
                break;
            case "p":
                spawnPlayer(tileArray[i].px, tileArray[i].py);
                break;
            case "f":
                let flag = new Entity("Flag");
                flag.cBoundingBox = new CBoundingBox(32, 32);
                flag.cAnimation = new CAnimation(new Animation(100, 100, 1, "./client/assets/flag.png"), true);
                flag.cTransform = new CTransform(tileArray[i].px, tileArray[i].py, 0, 0);
                entityArray.push(flag);
                break;
            case "crown": //crown for the last campaign level
                let crown = new Entity("Crown");
                crown.cBoundingBox = new CBoundingBox(16, 16);
                crown.cAnimation = new CAnimation(new Animation(112, 112, 1, "./client/assets/crown.png"), true);
                crown.cTransform = new CTransform(tileArray[i].px, tileArray[i].py, 0, 0);
                entityArray.push(crown);
                break;
            case "c":
                let coin = new Entity("Coin");
                coin.cBoundingBox = new CBoundingBox(24, 24);
                coin.cAnimation = new CAnimation(new Animation(16, 16, 5, "./client/assets/coin.png"), true);
                coin.cTransform = new CTransform(tileArray[i].px, tileArray[i].py, 0, 0);
                coin.cCollectible = new CCollectible();
                entityArray.push(coin);
                break;
            case "d"://d for drink = potion, p used for player
                let potion = new Entity("Potion");
                potion.cBoundingBox = new CBoundingBox(16, 16);
                potion.cAnimation = new CAnimation(new Animation(32, 32, 1, "./client/assets/drink.png"), true);
                potion.cTransform = new CTransform(tileArray[i].px, tileArray[i].py, 0, 0);
                potion.cCollectible = new CCollectible();
                entityArray.push(potion);
                break;
            case "h":
                let health = new Entity("Health");
                health.cBoundingBox = new CBoundingBox(16, 16);
                health.cAnimation = new CAnimation(new Animation(7, 8, 1, "./client/assets/health.png"), true);
                health.cTransform = new CTransform(tileArray[i].px, tileArray[i].py, 0, 0);
                health.cCollectible = new CCollectible();
                entityArray.push(health);
                break; //}
            case "t": //if (tileArray[i].ptype == "t") {
                let fireTrap = new Entity("Trap");
                fireTrap.cBoundingBox = new CBoundingBox(24, 24);
                fireTrap.cAnimation = new CAnimation(new Animation(100, 100, 8, "./client/assets/trapFire.png"), true);
                fireTrap.cTransform = new CTransform(tileArray[i].px, tileArray[i].py, 0, 0);
                entityArray.push(fireTrap);
                break; //}
            case "a": //if (tileArray[i].ptype == "a") {
                let actualBoss = new Entity("Boss");
                actualBoss.cBoundingBox = new CBoundingBox(96, 96);
                actualBoss.cAnimation = new CAnimation(new Animation(123, 123, 1, "./client/assets/actualboss.png"), true);
                actualBoss.cTransform = new CTransform(tileArray[i].px, tileArray[i].py, 0, 0);
                actualBoss.cHealth = new CHealth(500);
                //    actualBoss.cGravity = new CGravity();
                actualBoss.cFollow = new CFollow(2.5, tileArray[i].px, tileArray[i].py);
                actualBoss.cState = new CState("none");
                entityArray.push(actualBoss);

                break; //}
            case "m": //if (tileArray[i].ptype == "m") {
                let miniBoss = new Entity("MiniBoss");
                miniBoss.cBoundingBox = new CBoundingBox(32, 32);
                miniBoss.cAnimation = new CAnimation(new Animation(120, 120, 1, "./client/assets/miniboss.png"), true);
                miniBoss.cTransform = new CTransform(tileArray[i].px, tileArray[i].py, 0, 0);
                miniBoss.cHealth = new CHealth(50);
                //  miniBoss.cGravity = new CGravity();
                miniBoss.cFollow = new CFollow(2.5, tileArray[i].px, tileArray[i].py);
                miniBoss.cState = new CState("none");
                entityArray.push(miniBoss);
                break;

        }
    }

    backgroundSound = new sound("./client/assets/theme.wav");
    if (settings.musicOn === true) {
        backgroundSound.play();
    }
    myGameArea.start();
}

function keysDown(e) {
    switch (e.keyCode) {
        case keyBinds[7]:
            drawPauseMenu();
            break;
        case keyBinds[0]:
            input.leftInput = true;
            break;
        case keyBinds[1]:
            input.rightInput = true;
            break;
        case keyBinds[2]:
            input.upInput = true;
            break;
        case keyBinds[3]:
            input.downInput = true;
            break;
        case keyBinds[5]:
            input.actionInput = true;
            break;
        case keyBinds[4]:
            input.attackInput = true;
            break;
        case keyBinds[6]:
            input.weaponToggleInput = true;
            break;
        case 192:
            godMode = !godMode;
            toggleGodMode(godMode);

        //additional cases here
    }
}

function keysUp(e) {
    //left, right, jump, down, attack, action, weapon, pause
    switch (e.keyCode) {
        case keyBinds[0]:
            input.leftInput = false;
            break;
        case keyBinds[1]:
            input.rightInput = false;
            break;
        case keyBinds[2]:
            input.upInput = false;
            break;
        case keyBinds[3]:
            input.downInput = false;
            break;
        case keyBinds[5]:
            input.actionInput = false;
            break;
        case keyBinds[4]:
            input.attackInput = false;
            break;
        case keyBinds[6]:
            input.weaponToggleInput = false;
            break;
        //additional cases here
    }
}

let myGameArea = {
    gameCanvas: document.createElement("canvas"),
    start: function () {
        this.gameCanvas.style.display = 'block';
        this.gameCanvas.width = 1280;
        this.gameCanvas.height = 720;
        //+viewport
        viewport.screen = [this.gameCanvas.width, this.gameCanvas.height];
        //-viewport
        this.context = this.gameCanvas.getContext("2d");
        document.body.insertBefore(this.gameCanvas, document.body.childNodes[0]);
        this.interval = setInterval(updateGameArea, 35);

        // window.addEventListener('keydown', keysDown);
        //  window.addEventListener('keyup', keysUp);
    },

    stop: function () {
        clearInterval(myGameArea.interval);
    },

    clear: function () {
        this.context.clearRect(0, 0, this.gameCanvas.width, this.gameCanvas.height);
        //  this.context.fillStyle = "black";
        if (backgroundImage) {
            this.context.fillRect(0, 0, this.gameCanvas.width, this.gameCanvas.height);
            this.context.drawImage(backgroundImage,
                (this.gameCanvas.width / 2) - (backgroundImage.width / 2), 0,
                backgroundImage.width, this.gameCanvas.height);
        }
    }
}

function SMovement() {
    let i;
    let entitiesOutOfBounds = [];

    Player.cState.stateChanged = false;
    //setting player speed changes based on input. The user should quickly "bleed speed" when not inputting a given direction of actively walljumping
    if ((!input.leftInput && !input.rightInput)/* || 
        (input.leftInput  && Player.cTransform.xSpeed > 0) ||
        (input.rightInput && Player.cTransform.xSpeed < 0)*/) {
        Player.cTransform.xSpeed /= 2;
        if (Player.cState.state != "jumping") { Player.cState.state = "standing"; }
    }

    if (input.leftInput && !input.rightInput) {
        Player.cTransform.xSpeed -= .4;
        Player.cState.state = "runLeft";
    }

    if (input.rightInput && !input.leftInput) {
        Player.cTransform.xSpeed += .4;
        Player.cState.state = "runRight";
    }

    if (input.downInput && !input.upInput && godMode) {
        Player.cTransform.ySpeed += .4;
        //  Player.cState.state = "runRight";
    }



    if (Math.abs(Player.cTransform.xSpeed) <= .1) Player.cTransform.xSpeed = 0;
    if (input.upInput && Player.cTransform.ySpeed == 0 && Player.cState.prevState != "jumping") {
        Player.cTransform.ySpeed -= MAXSPEEDY; Player.cState.state = "jumping";
        if (settings.soundOn) {
            jumpSound.play();
        }
    }
    if (input.attackInput && Player.cState.state != "jumping") Player.cState.state = "attack";
    if (Player.cState.state != Player.cState.prevState) Player.cState.stateChanged = true;
    // console.log(Player.cState.state);
    Player.cState.prevState = Player.cState.state;

    for (i = 0; i < entityArray.length; i++) {
        //+viewport
        entityArray[i].cTransform.x += viewport.offset[0];
        if (entityArray[i].cTransform.x < viewport.start[0] || entityArray[i].cTransform.x > viewport.end[0]
            /* || entityArray[i].cTransform.y < viewport.start[1] || entityArray[i].cTransform.y > viewport.end[1] */) {
            continue;
        }
        //entityArray[i].cTransform.y += viewport.offset[1];
        //-viewport
        entityArray[i].cTransform.prevX = entityArray[i].cTransform.x;
        entityArray[i].cTransform.prevY = entityArray[i].cTransform.y;
        if (entityArray[i].cGravity) { // Gravity will not affect entities without a CGravity property, such as tiles
            entityArray[i].cTransform.ySpeed += gravity;
        }

        if (entityArray[i].cTransform.xSpeed > MAXSPEEDX) entityArray[i].cTransform.xSpeed = MAXSPEEDX;
        if (entityArray[i].cTransform.xSpeed < -1 * MAXSPEEDX) entityArray[i].cTransform.xSpeed = -1 * MAXSPEEDX;
        if (entityArray[i].cTransform.ySpeed > MAXSPEEDY) entityArray[i].cTransform.ySpeed = MAXSPEEDY;
        if (entityArray[i].cTransform.ySpeed < -1 * MAXSPEEDY) entityArray[i].cTransform.ySpeed = -1 * MAXSPEEDY;


        let intendedNextPositionX = entityArray[i].cTransform.x + entityArray[i].cTransform.xSpeed;
        let intendedNextPositionY = entityArray[i].cTransform.y + entityArray[i].cTransform.ySpeed;

        if (intendedNextPositionX <= 0 || intendedNextPositionX >= (myGameArea.gameCanvas.width - entityArray[i].cBoundingBox.width)) entityArray[i].cTransform.xSpeed = 0;
        if (intendedNextPositionY <= 0) entityArray[i].cTransform.ySpeed = 0;

        if (intendedNextPositionY >= (myGameArea.gameCanvas.height + entityArray[i].cBoundingBox.height / 2)) entitiesOutOfBounds.push(entityArray[i]);
        if (entityArray[i].type != "NPC") {
            entityArray[i].cTransform.x += entityArray[i].cTransform.xSpeed;
        }
        if (entityArray[i].type == "NPC") {
            if (entityArray[i].cTransform.xSpeed == 0) { entityArray[i].cState.state = "standing"; }
        }
        entityArray[i].cTransform.y += entityArray[i].cTransform.ySpeed;


    }

    let j;
    for (j = 0; j < entitiesOutOfBounds.length; j++) {
        destroyEntity(entitiesOutOfBounds[j])
    }

    //+viewport
    viewport.update(Player.cTransform.x + 16, Player.cTransform.y + 16);
    myGameArea.clear();
    //-viewport
}

function sound(src) {
    this.sound = document.createElement("audio");
    this.sound.src = src;
    this.sound.setAttribute("preload", "auto");
    this.sound.setAttribute("controls", "none");
    this.sound.style.display = "none";
    document.body.appendChild(this.sound);
    this.play = function () {
        this.sound.play();
    }
    this.stop = function () {
        this.sound.pause();
    }
}

function updateGameArea() {
    myGameArea.clear();

    if (Player.cTransform.xSpeed == 0) { Player.cState.state = "standing"; }

    // Update NPC states based on movement, or HP for dying 
    for (let i = 0; i < entityArray.length; i++) {
        //+viewport
        if (entityArray[i].cTransform.x < viewport.start[0] || entityArray[i].cTransform.x > viewport.end[0]
            /* || entityArray[i].cTransform.y < viewport.start[1] || entityArray[i].cTransform.y > viewport.end[1] */) {
            continue;
        }
        //-viewport
        if (entityArray[i].type == "NPC") {
            entityArray[i].cState.stateChanged = false;
            if (entityArray[i].cTransform.xSpeed < 0) { entityArray[i].cState.state == "runLeft"; }
            if (entityArray[i].cTransform.xSpeed > 0) { entityArray[i].cState.state == "runRight"; }
            if (entityArray[i].cTransform.xSpeed == 0) { entityArray[i].cState.state == "standing"; }
            // If we have sprites with dying animations 
            if (entityArray[i].cHealth.currentHP <= 0) {
                entityArray[i].cState.state == "dying";
                destroyEntity(entityArray[i]);
            }
            if (entityArray[i].cState.prevState != entityArray[i].cState.state) entityArray[i].cState.stateChanged = true;
            entityArray[i].cState.prevState = entityArray[i].cState.state;
        }
    }
    gameLoop();
}

function enemyCollision(playerOverlap, playerPrevOverlap, entityPrev, isBoss){
    if (isBoss){
        playerOverlap[0] *= 2;
        playerOverlap[1] *= 2;
    }
    // Player vs bottom
    if (playerPrevOverlap[0] > 0 && entityPrev[1] < Player.cTransform.prevY) {
        Player.cTransform.y += playerOverlap[1];
    }
    // Player vs top
    if (playerPrevOverlap[0] > 0 && entityPrev[1] > Player.cTransform.prevY) {
        Player.cTransform.y -= playerOverlap[1];
    }
    // Player vs left 
    if (playerPrevOverlap[1] > 0 && entityPrev[0] < Player.cTransform.prevX) {
        Player.cTransform.x += playerOverlap[0];
    }
    // Player vs right 
    if (playerPrevOverlap[1] > 0 && entityPrev[0] > Player.cTransform.prevX) {
        Player.cTransform.x -= playerOverlap[0];
    }
    Player.cTransform.xSpeed *= -1;
    Player.cTransform.ySpeed *= -1;
}

function collision() {
    for (let i = 0; i < entityArray.length; i++) {  //ONE pass through the array to check collision with player
        if (entityArray[i] == undefined) {
            continue;
        }
        //+viewport
        if (entityArray[i].cTransform.x < viewport.start[0] || entityArray[i].cTransform.x > viewport.end[0]
            /* || entityArray[i].cTransform.y < viewport.start[1] || entityArray[i].cTransform.y > viewport.end[1] */) {
            continue;
        }
        //-viewport
        console.log();
        let playerOverlap = [0, 0];
        let playerPrevOverlap = [0, 0];
        let playerPrevX = Player.cTransform.prevX;
        let playerPrevY = Player.cTransform.prevY;
        let entityPrev = [0, 0];
        playerOverlap = GetOverLap(entityArray[i], Player);
        playerPrevOverlap = GetPreviousOverlap(entityArray[i], Player);
        entityPrev[0] = entityArray[i].cTransform.prevX;
        entityPrev[1] = entityArray[i].cTransform.prevY;
        if (playerOverlap[0] > 0 && playerOverlap[1] > 0) { // Check if there is any overlap at all 
            //case: Player colliding with a TRAP
            if (entityArray[i].cTrap) destroyEntity(Player);

            //CASE: Player colliding with a TILE
            if (entityArray[i].type == "Tile") { // Collision between player and tiles 
                // Player vs tile bottom 

                if (playerPrevOverlap[0] > 0 && entityPrev[1] < playerPrevY) {
                    Player.cTransform.y += playerOverlap[1];
                    Player.cTransform.ySpeed = 0;

                }
                // Player vs tile top
                if (playerPrevOverlap[0] > 0 && entityPrev[1] > playerPrevY) {
                    Player.cTransform.y -= playerOverlap[1];
                    Player.cTransform.ySpeed = 0;

                }
                // Player vs tile left 
                if (playerPrevOverlap[1] > 0 && entityPrev[0] < playerPrevX) {
                    Player.cTransform.x += playerOverlap[0];

                    if (input.upInput && Player.cTransform.ySpeed <= 0) {
                        //Player.cTransform.x += 8;
                        Player.cTransform.xSpeed *= -1;
                        Player.cTransform.ySpeed = -10;
                    }

                }
                // Player vs tile right 
                if (playerPrevOverlap[1] > 0 && entityPrev[0] > playerPrevX) {
                    Player.cTransform.x -= playerOverlap[0];

                    if (input.upInput && Player.cTransform.ySpeed <= 0) {
                        //Player.cTransform.x -= 8;
                        Player.cTransform.xSpeed *= -1;
                        Player.cTransform.ySpeed = -10;
                    }

                }
            }

            // NPC vs Player


            else if (entityArray[i].type == "NPC") {
                // If NPC and player collide, player will lose HP 
                if (playerOverlap[0] > 0 && playerOverlap[1] > 0 && Player.cHealth.currentHP > 0) {
                    if (Player.cState.state == "attack") {
                        destroyEntity(entityArray[i]);
                        Player.cScore.score += 50; // Player gains 50 score for killing an enemy 
                    } else if (!tookDamage) {
                        Player.cHealth.currentHP -= 15;
                        tookDamage = true;

                        if (settings.soundOn) {
                            HitSound.play();
                        }

                    }
                    if (!godMode) enemyCollision(playerOverlap, playerPrevOverlap, entityPrev, false);

                    if (Player.cHealth.currentHP <= 0) {
                        playerHPIsDepleted = true;
                    }
                }
            }

            // Player vs miniboss
            else if (entityArray[i].type == "MiniBoss") {
                if (Player.cState.state == "attack") {
                    if (entityArray[i].cHealth.currentHP > 0) {
                        // Each hit to the mini boss from player does 10 damage 
                        entityArray[i].cHealth.currentHP -= 10;
                    }
                } else if (Player.cHealth.currentHP > 0 && !tookDamage) {
                    // mini boss hits player for 10hp, harder than regular enemies 
                    Player.cHealth.currentHP -= 15;
                    tookDamage = true;
                }
                if (!godMode) enemyCollision(playerOverlap, playerPrevOverlap, entityPrev, false);
                if (entityArray[i].cHealth.currentHP <= 0) { destroyEntity(entityArray[i]); } // Kill boss when HP reaches 0 
                
                if (Player.cHealth.currentHP <= 0) {
                    playerHPIsDepleted = true;
                }
            }

            // Player vs actual boss 
            else if (entityArray[i].type == "Boss") {
                if (Player.cState.state == "attack") {
                    if (entityArray[i].cHealth.currentHP > 0) {
                        // Each hit to the boss from player does 20 damage 
                        entityArray[i].cHealth.currentHP -= 10;
                    }
                } else if (Player.cHealth.currentHP > 0 && !tookDamage) {
                    // boss hits player for 20hp, harder than regular enemies or mini boss 
                    Player.cHealth.currentHP -= 25;
                    tookDamage = true;
                }
                if (!godMode) enemyCollision(playerOverlap, playerPrevOverlap, entityPrev, true);
                if (entityArray[i].cHealth.currentHP <= 0) { destroyEntity(entityArray[i]); } // Kill boss when HP reaches 0 

                if (Player.cHealth.currentHP <= 0) {
                    playerHPIsDepleted = true;
                }
            }
            // Player vs flag 
            else if (entityArray[i].type == "Flag") {
                backgroundSound.stop();
                if (settings.soundOn) {
                    levelFinishSound.play();
                }
                if (gameEngineLevelID > 10) {
                    let finalSeconds = (minutes * 60) + (seconds);
                    sock.emit('saveHighscore', { levelID: gameEngineLevelID, userID: userID, time: finalSeconds, score: Player.cScore.score });
                } else if (gameEngineLevelID === storyProgress + 1) {
                    storyProgress = storyProgress + 1;
                    sock.emit('saveCampaign', { userID: userID, saveState: storyProgress });
                }
                endGameState();
                break;
            }
            // Player vs crown 
            else if (entityArray[i].type == "Crown") {
                backgroundSound.stop();
                if (settings.soundOn) {
                    levelFinishSound.play();
                }
                if (gameEngineLevelID > 10) {
                    let finalSeconds = (minutes * 60) + (seconds);
                    sock.emit('saveHighscore', { levelID: gameEngineLevelID, userID: userID, time: finalSeconds, score: Player.cScore.score });
                    endGameState();
                } else if (gameEngineLevelID <= storyProgress + 1) {
                    storyProgress = storyProgress + 1;
                    sock.emit('saveCampaign', { userID: userID, saveState: storyProgress });
                    drawEndOfCampaign();
                } else {
                    endGameState();
                }
            }

            // Player vs a trap 
            else if (entityArray[i].type == "Trap") {
                // Traps will instantly kill a player
                if (!godMode){ 
                    Player.cHealth.currentHP = 0;
                    playerHPIsDepleted = true;
                }
            }

            //CASE: Player collides with a collectible object
            else if (entityArray[i].cCollectible) {
                // Coin entity 
                if (entityArray[i].type == "Coin") {
                    // If a player collides with a coin, increase score by 50 
                    Player.cScore.score += 50;
                    // Once the coin is 'collected', destroy it 
                    destroyEntity(entityArray[i]);
                }
                // Health entity 
                else if (entityArray[i].type == "Health") {
                    // If a player collides with health, increase HP by 50
                    Player.cHealth.currentHP += 50;
                    if (Player.cHealth.currentHP > Player.cHealth.maxHP) Player.cHealth.currentHP = Player.cHealth.maxHP;
                    destroyEntity(entityArray[i]);
                }
                // Potion entity 
                else if (entityArray[i].type == "Potion") {
                    //potion shields the player for 10s with temp hp
                    startHP = Player.cHealth.currentHP;
                    Player.cHealth.currentHP += 30;
                    destroyEntity(entityArray[i]);
                    setTimeout(function(){
                        //remove shield after 10s, unless the temp hp has already been taken as damage
                        if (Player.cHealth.currentHP > startHP) Player.cHealth.currentHP = startHP;
                        if (Player.cHealth.currentHP > Player.cHealth.maxHP) Player.cHealth.currentHP = Player.cHealth.maxHP;
                    }, 10000);
                }
            }
            /*
            //CASE: Player collides with an interactable object AND is attempting to USE
            if (entityArray[i].CInteractable && input.actionInput) {
                entityArray[i].CInteractable.action;
            }
            */
        }

        if (playerHPIsDepleted) {
            destroyEntity(Player);
        }
    }
    let playerLessArray = [];
    for (let k = 0; k < entityArray.length; k++) {
        if (entityArray[k].id != Player.id) playerLessArray.push(entityArray[k]);
    }
    for (let j = 0; j < playerLessArray.length; j++) {  //TWO nested passes through the array to check non-player collisions
        //+viewport
        if (playerLessArray[j].cTransform.x < viewport.start[0] || playerLessArray[j].cTransform.x > viewport.end[0]
            /* || playerLessArray[j].cTransform.y < viewport.start[1] || playerLessArray[j].cTransform.y > viewport.end[1] */) {
            continue;
        }
        //-viewport

        // npc's should move through these, and they have no gravity so they won't fall through anything
        if (playerLessArray[i].type == "Coin" || playerLessArray[i].type == "Potion" || playerLessArray[i].type == "Health" ||
            playerLessArray[i].type == "Trap" || playerLessArray[i].type == "Flag" || playerLessArray[i].type == "Crown"){
                continue;
        }

        for (let i = 0; i < playerLessArray.length; i++) {
            // npc's should move through these, and they have no gravity so they won't fall through anything
            if (playerLessArray[j].type == "Coin" || playerLessArray[j].type == "Potion" || playerLessArray[j].type == "Health" ||
                playerLessArray[j].type == "Trap" || playerLessArray[j].type == "Flag" || playerLessArray[j].type == "Crown"){
                    continue;
            }
            if (playerLessArray[i].type == "NPC" || playerLessArray[i].type == "MiniBoss" || playerLessArray[i].type == "Boss") {

                let npcOverlap = [0, 0];
                let npcPrevOverlap = [0, 0];
                let npcPrevX = playerLessArray[i].cTransform.prevX;
                let npcPrevY = playerLessArray[i].cTransform.prevY;
                let entityPrev = [0, 0];
                npcOverlap = GetOverLap(playerLessArray[j], playerLessArray[i]);
                npcPrevOverlap = GetPreviousOverlap(playerLessArray[j], playerLessArray[i]);
                entityPrev[0] = playerLessArray[j].cTransform.prevX;
                entityPrev[1] = playerLessArray[j].cTransform.prevY;

                if (npcOverlap[0] > 0 && npcOverlap[1] > 0) { // Any overlap at all 

                    // NPC vs tile bottom 
                    if (npcPrevOverlap[0] > 0 && entityPrev[1] < npcPrevY) {
                        playerLessArray[i].cTransform.y += npcOverlap[1];
                        playerLessArray[i].cTransform.ySpeed = 0;

                    }
                    // NPC vs tile top
                    if (npcPrevOverlap[0] > 0 && entityPrev[1] > npcPrevY) {
                        playerLessArray[i].cTransform.y -= npcOverlap[1];
                        playerLessArray[i].cTransform.ySpeed = 0;

                    }
                    // NPC vs tile left
                    if (npcPrevOverlap[1] > 0 && entityPrev[0] < npcPrevX) {
                        playerLessArray[i].cTransform.x += npcOverlap[0];
                        playerLessArray[i].cTransform.xSpeed = 0;

                    }
                    // NPC vs tile right
                    if (npcPrevOverlap[1] > 0 && entityPrev[0] > npcPrevX) {
                        playerLessArray[i].cTransform.x -= npcOverlap[0];
                        playerLessArray[i].cTransform.xSpeed = 0;

                    }
                }
            }

        }

    }
}
/* -------------- Physics -------------------------------- */
function GetOverLap(e1, e2) {

    let delta = [0, 0]; // index 0 is X index 1 is Y 
    let x1 = e1.cTransform.x;
    let x2 = e2.cTransform.x;

    let y1 = e1.cTransform.y;
    let y2 = e2.cTransform.y;

    let w1 = e1.cBoundingBox.width;
    let h1 = e1.cBoundingBox.height;

    let w2 = e2.cBoundingBox.width;
    let h2 = e2.cBoundingBox.height;

    delta[0] = Math.abs(x1 - x2);
    delta[1] = Math.abs(y1 - y2);

    let ox = (w1 / 2) + (w2 / 2) - delta[0];
    let oy = (h1 / 2) + (h2 / 2) - delta[1];

    delta[0] = ox;
    delta[1] = oy;

    return delta;
}

function GetPreviousOverlap(e1, e2) {
    let delta = [0, 0]; // index 0 is X index 1 is Y 
    let x1 = e1.cTransform.prevX;
    let x2 = e2.cTransform.prevX;

    let y1 = e1.cTransform.prevY;
    let y2 = e2.cTransform.prevY;
    let w1 = e1.cBoundingBox.width / 2;
    let h1 = e1.cBoundingBox.height / 2;

    let w2 = e2.cBoundingBox.width / 2;
    let h2 = e2.cBoundingBox.height / 2;

    delta[0] = Math.abs(x1 - x2);
    delta[1] = Math.abs(y1 - y2);

    let prev_ox = (w1 / 2) + (w2 / 2) - delta[0];
    let prev_oy = (h1 / 2) + (h2 / 2) - delta[1];

    delta[0] = prev_ox;
    delta[1] = prev_oy;

    return delta;
}

function getDistance(e1, e2) {
    // Distance formula to calculate distance between two enemies
    let distanceX = Math.pow((e1.cTransform.x - e2.cTransform.x), 2);
    let distanceY = Math.pow((e1.cTransform.y - e2.cTransform.y), 2);

    let realDistance = Math.sqrt(distanceX + distanceY);

    return realDistance;
}
function AI() {
    for (let i = 0; i < entityArray.length; i++) {
        //+viewport
        if (entityArray[i].cTransform.x < viewport.start[0] || entityArray[i].cTransform.x > viewport.end[0]
            /* || entityArray[i].cTransform.y < viewport.start[1] || entityArray[i].cTransform.y > viewport.end[1] */) {
            continue;
        }
        //-viewport
        if (entityArray[i].type == "NPC" || entityArray[i].type == "Boss" || entityArray[i].type == "MiniBoss") {
            if (entityArray[i].cFollow) {

                let distance = getDistance(Player, entityArray[i]);

                // Changing distance < VALUE will change enemy aggro distance 
                if (distance < 250) {
                    if (Player.cTransform.x > entityArray[i].cTransform.x) {
                        entityArray[i].cState.state = "runRight";
                        entityArray[i].cTransform.x += entityArray[i].cFollow.speed; // change speed at which they chase you 

                    }
                    if (Player.cTransform.x < entityArray[i].cTransform.x) {
                        entityArray[i].cState.state = "runLeft";
                        entityArray[i].cTransform.x -= entityArray[i].cFollow.speed; // Change speed at which they chase you      
                    }
                    // Bosses and minibosses will fly around to chase you 
                    if (entityArray[i].type == "Boss" || entityArray[i].type == "MiniBoss") {
                        if (Player.cTransform.y > entityArray[i].cTransform.y) {
                            entityArray[i].cTransform.y += 1;
                        }
                        if (Player.cTransform.y < entityArray[i].cTransform.y) {
                            entityArray[i].cTransform.y -= 1;
                        }
                    }

                }

                if (entityArray[i].cState.state == "standing") {
                    // Standing animation for enemy 
                }

            }
        }
    }
}

function toggleGodMode(godMode) {
    if (godMode == true) {
        Player.cHealth.currentHP = 9000000000000000;
        Player.cHealth.maxHP = 9000000000000000;
        Player.cGravity = null;
        console.log("God mode enabled");
    }
    else if (godMode == false) {
        Player.cHealth.currentHP = 100;
        Player.cHealth.maxHP = 100;
        Player.cGravity = new CGravity();
        console.log("God mode disabled");
    }
}

function drawEndOfCampaign() {
    clearInterval(myGameArea.interval);
    clearInterval(timer);
    setTimeout(function () {
        myGameArea.gameCanvas.addEventListener('click', endOfCampaignListener);
        radius = 10;
        for (i = 0; i < 3; i++) {
            myGameArea.context.beginPath();
            myGameArea.context.moveTo(endCampaignButtons[i].x + radius, endCampaignButtons[i].y);
            myGameArea.context.lineTo(endCampaignButtons[i].x + endCampaignButtons[i].width - radius, endCampaignButtons[i].y);
            myGameArea.context.quadraticCurveTo(endCampaignButtons[i].x + endCampaignButtons[i].width, endCampaignButtons[i].y, endCampaignButtons[i].x + endCampaignButtons[i].width, endCampaignButtons[i].y + radius);
            myGameArea.context.lineTo(endCampaignButtons[i].x + endCampaignButtons[i].width, endCampaignButtons[i].y + endCampaignButtons[i].height - radius);
            myGameArea.context.quadraticCurveTo(endCampaignButtons[i].x + endCampaignButtons[i].width, endCampaignButtons[i].y + endCampaignButtons[i].height, endCampaignButtons[i].x + endCampaignButtons[i].width - radius, endCampaignButtons[i].y + endCampaignButtons[i].height);
            myGameArea.context.lineTo(endCampaignButtons[i].x + radius, endCampaignButtons[i].y + endCampaignButtons[i].height);
            myGameArea.context.quadraticCurveTo(endCampaignButtons[i].x, endCampaignButtons[i].y + endCampaignButtons[i].height, endCampaignButtons[i].x, endCampaignButtons[i].y + endCampaignButtons[i].height - radius);
            myGameArea.context.lineTo(endCampaignButtons[i].x, endCampaignButtons[i].y + radius);
            myGameArea.context.quadraticCurveTo(endCampaignButtons[i].x, endCampaignButtons[i].y, endCampaignButtons[i].x + radius, endCampaignButtons[i].y);
            myGameArea.context.fillStyle = '#FFFFFF';
            myGameArea.context.fillStyle = 'rgba(124,193,255,1.0)'; // Color of Buttons
            myGameArea.context.fill();
            myGameArea.context.lineWidth = 2;
            myGameArea.context.strokeStyle = '#757BE8'; //Color Of Box Outline
            myGameArea.context.stroke();
            myGameArea.context.closePath();
            myGameArea.context.fillStyle = '#000000'; //Color of Text
            myGameArea.context.textAlign = 'center';
            myGameArea.context.font = '40pt Impact';
            if (endCampaignButtons[i].wrap === true) {
                let buttonText = endCampaignButtons[i].text.split(' ');
                buttonText[0] = buttonText[0].concat(' ');
                buttonText[0] = buttonText[0].concat(buttonText[1]);
                myGameArea.context.fillText(buttonText[0], endCampaignButtons[i].x + endCampaignButtons[i].width / 2, endCampaignButtons[i].y + (endCampaignButtons[i].height * .48));
                myGameArea.context.fillText(buttonText[2], endCampaignButtons[i].x + endCampaignButtons[i].width / 2, endCampaignButtons[i].y + (endCampaignButtons[i].height * .72));
            } else {
                myGameArea.context.fillText(endCampaignButtons[i].text, endCampaignButtons[i].x + endCampaignButtons[i].width / 2, endCampaignButtons[i].y + (endCampaignButtons[i].height * .6));
            }
        }
        myGameArea.context.fillText("Congratulations, " + userID + ",", 600, 60);
        myGameArea.context.fillText("you've retrieved the crown and saved the kingdom!", 600, 120);
        myGameArea.context.fillText("Well done!", 600, 180);
    }, 150);
}
function endOfCampaignListener(evt) {
    let mousePos = getMousePos(myGameArea.gameCanvas, evt);
    backgroundSound.stop();
    if (isInside(mousePos, endCampaignButtons[1])) {
        endGameState();
        myGameArea.gameCanvas.removeEventListener('click', endOfCampaignListener);
    } else if (isInside(mousePos, endCampaignButtons[2])) {
        startGame(entityArray2);
        myGameArea.gameCanvas.removeEventListener('click', endOfCampaignListener);
    }
}

function drawPauseMenu() {
    clearInterval(myGameArea.interval);
    clearInterval(timer);
    myGameArea.gameCanvas.addEventListener('click', pauseMenuListener);
    radius = 10;
    for (i = 0; i < 3; i++) {
        myGameArea.context.beginPath();
        myGameArea.context.moveTo(pauseMenuButtons[i].x + radius, pauseMenuButtons[i].y);
        myGameArea.context.lineTo(pauseMenuButtons[i].x + pauseMenuButtons[i].width - radius, pauseMenuButtons[i].y);
        myGameArea.context.quadraticCurveTo(pauseMenuButtons[i].x + pauseMenuButtons[i].width, pauseMenuButtons[i].y, pauseMenuButtons[i].x + pauseMenuButtons[i].width, pauseMenuButtons[i].y + radius);
        myGameArea.context.lineTo(pauseMenuButtons[i].x + pauseMenuButtons[i].width, pauseMenuButtons[i].y + pauseMenuButtons[i].height - radius);
        myGameArea.context.quadraticCurveTo(pauseMenuButtons[i].x + pauseMenuButtons[i].width, pauseMenuButtons[i].y + pauseMenuButtons[i].height, pauseMenuButtons[i].x + pauseMenuButtons[i].width - radius, pauseMenuButtons[i].y + pauseMenuButtons[i].height);
        myGameArea.context.lineTo(pauseMenuButtons[i].x + radius, pauseMenuButtons[i].y + pauseMenuButtons[i].height);
        myGameArea.context.quadraticCurveTo(pauseMenuButtons[i].x, pauseMenuButtons[i].y + pauseMenuButtons[i].height, pauseMenuButtons[i].x, pauseMenuButtons[i].y + pauseMenuButtons[i].height - radius);
        myGameArea.context.lineTo(pauseMenuButtons[i].x, pauseMenuButtons[i].y + radius);
        myGameArea.context.quadraticCurveTo(pauseMenuButtons[i].x, pauseMenuButtons[i].y, pauseMenuButtons[i].x + radius, pauseMenuButtons[i].y);
        myGameArea.context.fillStyle = '#FFFFFF';
        myGameArea.context.fillStyle = 'rgba(124,193,255,1.0)'; // Color of Buttons
        myGameArea.context.fill();
        myGameArea.context.lineWidth = 2;
        myGameArea.context.strokeStyle = '#757BE8'; //Color Of Box Outline
        myGameArea.context.stroke();
        myGameArea.context.closePath();
        myGameArea.context.fillStyle = '#000000'; //Color of Text
        myGameArea.context.textAlign = 'center';
        myGameArea.context.font = '40pt Impact';
        if (pauseMenuButtons[i].wrap === true) {
            let buttonText = pauseMenuButtons[i].text.split(' ');
            buttonText[0] = buttonText[0].concat(' ');
            buttonText[0] = buttonText[0].concat(buttonText[1]);
            myGameArea.context.fillText(buttonText[0], pauseMenuButtons[i].x + pauseMenuButtons[i].width / 2, pauseMenuButtons[i].y + (pauseMenuButtons[i].height * .48));
            myGameArea.context.fillText(buttonText[2], pauseMenuButtons[i].x + pauseMenuButtons[i].width / 2, pauseMenuButtons[i].y + (pauseMenuButtons[i].height * .72));
        } else {
            myGameArea.context.fillText(pauseMenuButtons[i].text, pauseMenuButtons[i].x + pauseMenuButtons[i].width / 2, pauseMenuButtons[i].y + (pauseMenuButtons[i].height * .6));
        }
    }
}
function pauseMenuListener(evt) {
    let mousePos = getMousePos(myGameArea.gameCanvas, evt);
    backgroundSound.stop();
    if (isInside(mousePos, pauseMenuButtons[1])) {
        myGameArea.gameCanvas.addEventListener('click', pauseMenuListener);
        endGameState();
    } else if (isInside(mousePos, pauseMenuButtons[2])) {
        if (settings.musicOn) {
            backgroundSound.play();
        }
        myGameArea.gameCanvas.addEventListener('click', pauseMenuListener);
        myGameArea.clear();
        myGameArea.start();
        timer = setInterval(startTimer, 35);
    }
}
function endGameState() {
    clearInterval(myGameArea.interval);
    clearInterval(timer);
    backgroundSound.stop();
    if (gameStartedFrom === 0) {
        myGameArea.gameCanvas.style.display = 'none';
        document.getElementById('menuDiv').style.display = 'block';
        position = [];
        buffer_array = [];
        background_image = [];
    } else {
        myGameArea.gameCanvas.style.display = 'none';
        document.getElementById('levelEditorDiv').style.display = 'block';
    }
    myGameArea.clear()
    gameEngineLevelID = 0;
}

function updateHUD() {
    // Timer display
    myGameArea.context.font = "20px Georgia";
    myGameArea.context.fillStyle = "red";
    myGameArea.context.fillText("Time " + minutes + " : " + seconds, 10, 19);
    // Score display 
    myGameArea.context.font = "20px Georgia";
    myGameArea.context.fillStyle = "red";
    myGameArea.context.fillText("Score: " + Player.cScore.score, 10, 40);
    // HP display
    myGameArea.context.font = "20px Georgia";
    myGameArea.context.fillStyle = "red";
    myGameArea.context.fillText("HP: " + Player.cHealth.currentHP, 10, 61);

}

function destroyEntity(e) {
    let i;
    for (i = 0; i < entityArray.length; i++) {
        if (entityArray[i].id == e.id) {
            entityArray.splice(i, 1);
            entitiesToDestroy.push(e);
        }
    }

}

/* -------------- Animation ------------------  */

let SAnimation = function () {
    let i;
    for (i = 0; i < entityArray.length; i++) {
        //+viewport
        if (entityArray[i].cTransform.x < viewport.start[0] || entityArray[i].cTransform.x > viewport.end[0]
            /* || entityArray[i].cTransform.y < viewport.start[1] || entityArray[i].cTransform.y > viewport.end[1] */) {
            continue;
        }
        //-viewport
        drawSprites(entityArray[i]);
    }
}

function drawSprites(e) {
    if (!e.cAnimation) {
        console.log(e.id + " does not have a CAnimation");
        return;
    }
    let facing = (e.cTransform.xSpeed >= 0) ? 1 : -1;
    let anim = e.cAnimation.animation;      //TODO: this switch based on state should only affect Player.cAnimation.animation
    let a;
    if (e.id == Player.id && Player.cState.stateChanged) {
        if (Player.cState.state == "attack" && facing > 0) {
            a = new Animation(64, 64, 8, Player.cMultipleAnimationSet.animations[4]);
            Player.cAnimation = new CAnimation(a, true);
            anim = Player.cAnimation.animation;

        }
        else if (Player.cState.state == "attack" && facing < 0) {
            a = new Animation(64, 64, 8, Player.cMultipleAnimationSet.animations[3]);
            Player.cAnimation = new CAnimation(a, true);
            anim = Player.cAnimation.animation;


        }
        else if (Player.cState.state == "standing") {
            a = new Animation(64, 64, 1, Player.cMultipleAnimationSet.animations[0]);
            Player.cAnimation = new CAnimation(a, true);
            anim = Player.cAnimation.animation;

        }
        else if (Player.cState.state == "runRight") {
            a = new Animation(64, 64, 9, Player.cMultipleAnimationSet.animations[2])
            Player.cAnimation = new CAnimation(a, true);
            anim = Player.cAnimation.animation;

        }
        else if (Player.cState.state == "runLeft") {
            a = new Animation(64, 64, 9, Player.cMultipleAnimationSet.animations[1]);
            Player.cAnimation = new CAnimation(a, true);
            anim = Player.cAnimation.animation;

        }
        /*else if (Player.cState.state == "jumping") {
            Player.cAnimation = new CAnimation(Player.cMultipleAnimationSet.animations[0], true);
            anim = Player.cAnimation.animation;
            myGameArea.context.drawImage(anim.image, anim.currentFrame * anim.frameWidth, 0, 
                                            anim.frameWidth, anim.frameHeight, 
                                            e.cTransform.x, e.cTransform.y, 
                                            e.cBoundingBox.width, e.cBoundingBox.height)
        }*/

    }

    if (e.type == "NPC" && e.cState.stateChanged) {
        if (e.cState.state == "runLeft") {
            a = new Animation(64, 64, 9, e.cMultipleAnimationSet.animations[0]);
            e.cAnimation = new CAnimation(a, true);
        } else if (e.cState.state == "runRight") {
            a = new Animation(64, 64, 9, e.cMultipleAnimationSet.animations[1]);
            e.cAnimation = new CAnimation(a, true);
        }
        anim = e.cAnimation.animation;
    }

    myGameArea.context.drawImage(anim.image, anim.currentFrame * anim.frameWidth, 0,
        anim.frameWidth, anim.frameHeight,
        e.cTransform.x + (32 - e.cBoundingBox.width) / 2, e.cTransform.y + (32 - e.cBoundingBox.width) / 2,
        e.cBoundingBox.width, e.cBoundingBox.height);
    anim.update();

}

function gameLoop() {
    while (entitiesToDestroy.length > 0) {
        let e = entitiesToDestroy.pop();
        if (e.type == "Player") {
            myGameArea.clear();
            clearInterval(myGameArea.interval);
            startGame(entityArray2);
        }
    }
    SMovement();
    AI();
    collision();
    SAnimation();
    updateHUD();
}