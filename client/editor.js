/*
Level editor script for game engine
=============================
:Author: Timilehin Samuel Babalola
:Created: 2019-01-15
:Contributors: Greg Moores
               Josh Clarke

*/

var canvas = document.getElementById("canvas");
var ctx = canvas.getContext("2d");
var BB = canvas.getBoundingClientRect();
var offsetX = BB.left;
var offsetY = BB.top;
var WIDTH = canvas.width;
var HEIGHT = canvas.height;
var x = 0;
let levelEditorLevelID = null;
let socket = io();
var y = 0;
p = 0;
let img = "";
let isDragging = false;
let canvasLeft = canvas.offsetLeft;
let canvasTop = canvas.offsetTop;
let cellwidth = 32;
let cellheight = 32;
let position = [];
let background_image = [];
let buffer_array = [];
document.getElementById('levelEditorDiv').style.display = 'none';
let levelEditorLevelNameField = document.getElementById('levelEditorLevelNameField');
let levelEditorLevelDescField = document.getElementById('levelEditorLevelDescField');
let saveMenuButtons = [{ x: 150, y: 100, width: 700, height: 350, text: '' }, { x: 175, y: 325, width: 315, height: 100, text: 'Cancel' }, { x: 510, y: 325, width: 315, height: 100, text: 'Save' }];
let tileToPlace = [null,null];
let holdTile = true;
let clicks = 0;

canvas.ondrop = drop;
canvas.ondragover = allowDrop;
canvas.onmousedown = handleMouseDown;

drawGrid();
function player() {
  tileToPlace[1] = document.getElementById(event.srcElement);
  var base_image1 = document.getElementById(event.srcElement.id);
  tileToPlace[0] = base_image1;
  base_image1.onmousedown = mousedown;
  base_image1.ondragstart = drag;
}

function backToMenu() {
  if (previousMenuState === 7) {
    drawMenu(editLevelButtons, 8)
  } else {
    drawMenu(createMenuButtons, 7);
  }
  levelEditorLevelID = null;
  document.getElementById('levelEditorDiv').style.display = 'none';
  document.getElementById('menuDiv').style.display = 'block';
  ctx.clearRect(0,0,WIDTH, HEIGHT);
  drawGrid();
  position = [];
  buffer_array = [];
  screen = 1;
}

function playButtonClicked() {
  //only start game if there's a player
  for (var i = 0; i < position.length; i++) {
    pose = position[i];
    if (pose.ptype == 'p') {
      document.getElementById('levelEditorDiv').style.display = 'none';
      startGame(position);
    }
  }
  ctx.clearRect(0,0,WIDTH, HEIGHT);
  drawGrid();
  position = [];
  buffer_array = [];
  screen = 1;
}

function background_drop() {
  holdTile = false;
  document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function (event) {
  if (!event.target.matches('.dropbtn')) {
    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}

function player_drop() {
  holdTile = false;
  document.getElementById("myDropdown2").classList.toggle("shown");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function (event) {
  if (!event.target.matches('.dropbtn2')) {
    var dropdowns = document.getElementsByClassName("dropdown-content2");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('shown')) {
        openDropdown.classList.remove('shown');
      }
    }
  }
}

function enemy_drop() {
  holdTile = false;
  document.getElementById("myDropdown3").classList.toggle("show2");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function (event) {
  if (!event.target.matches('.dropbtn3')) {
    var dropdowns = document.getElementsByClassName("dropdown-content3");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show2')) {
        openDropdown.classList.remove('show2');
      }
    }
  }
}

function incentive_drop() {
  holdTile = false;
  document.getElementById("myDropdown4").classList.toggle("show3");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function (event) {
  if (!event.target.matches('.dropbtn4')) {
    var dropdowns = document.getElementsByClassName("dropdown-content4");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show3')) {
        openDropdown.classList.remove('show3');
      }
    }
  }
}

function brick_drop() {
  holdTile = false;
  document.getElementById("myDropdown5").classList.toggle("show4");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function (event) {
  if (!event.target.matches('.dropbtn5')) {
    var dropdowns = document.getElementsByClassName("dropdown-content5");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show5')) {
        openDropdown.classList.remove('show5');
      }
    }
  }
}

let my_margin = 0;
let screen = 1;
let base_image1 = new Image(WIDTH, HEIGHT);
function rightScroll() {
  holdTile = false;
  screen += 1;
  //base_image1.src = './client/assets/desertBack.png';
  tr = './client/assets/';
  if (background_image[0] === undefined){
    alert("Choose a background before moving to a new screen.");
    return;
  }
  base_image1.src = tr.concat(String(background_image[0].bname));
  base_image1.onload = function () {
    ctx.drawImage(base_image1, 0, 0, WIDTH, HEIGHT);
    drawGrid();
    if (buffer_array.length == 0) {
      redraw_loaded(position);
    } else if (buffer_array.length > 0) {
      redraw_loaded(buffer_array);
    }
  }
  if (screen >= 4) {
    alert("3 screen limit");
    screen -= 1;
  }
  alert("Current screen: " + screen);
}

function leftScroll(){
  holdTile = false;
  screen -=1;
    //base_image1.src = './client/assets/desertBack.png';
  tr = './client/assets/';
  if (background_image[0] === undefined){
    alert("Choose a background before moving to a new screen.");
    return;
  }
  base_image1.src = tr.concat(String(background_image[0].bname));
  base_image1.onload = function () {
    ctx.drawImage(base_image1, 0, 0, WIDTH, HEIGHT);
    drawGrid();
    if (buffer_array.length == 0){
      redraw_loaded(position);
    } else if(buffer_array.length > 0){ 
      redraw_loaded(buffer_array);
    }   
  }
  if(screen <= 0){
    alert("3 screen limit");
    screen +=1;
  }
  alert("Current screen: " + screen);
}

function background() {
  holdTile = false;
  //alert("Here!");
  ctx.clearRect(0, 0, WIDTH, HEIGHT);
  base_image1 = new Image();
  base_image1.src = document.getElementById(event.srcElement.id).src;
  back_name = base_image1.src.split("/");
  back_name = back_name[back_name.length - 1];
  base_image1.onload = function () {
    background_image = [];
    background_image.push({ bimg: base_image1, bname: back_name, type: 'b' });
    position.push({
      px: 0, py: 0, pimg: background_image[0].bimg, ptype:
        background_image[0].type, pname: background_image[0].bname
    });
    ctx.drawImage(base_image1, 0, 0, WIDTH, HEIGHT);
    drawGrid();
    redraw_loaded(position);
  }
}


function drawGrid() {
  ctx.strokeStyle = '#000000';
  ctx.beginPath();
  for (var x = 0; x <= WIDTH; x += cellwidth) {
    ctx.moveTo(x, p);
    ctx.lineTo(x, WIDTH)
    ctx.stroke();
  }
  for (var y = 0; y <= WIDTH; y += cellwidth) {
    ctx.moveTo(p, y);
    ctx.lineTo(WIDTH, y)
    ctx.stroke();
  }
}

var startOffsetX, startOffsetY;

function allowDrop(ev) {
  ev.preventDefault();
}


function mousedown(ev) {
  startOffsetX = ev.offsetX;
  startOffsetY = ev.offsetY;
}

function drag(ev) {
  ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
  ev.preventDefault();

  var dropX = ev.clientX - canvasLeft - startOffsetX;
  var dropY = ev.clientY - canvasTop - startOffsetY;
  var id = ev.dataTransfer.getData("Text");
  var dropElement = document.getElementById(id);
  let new_image = new Image();
  new_image.src = dropElement.src;
  let entity_name = new_image.src.split('/');
  // draw the drag image at the drop coordinates
  bufferX = Math.round(dropX / cellwidth) * cellheight;
  bufferY = Math.round(dropY / cellwidth) * cellheight;
  let type = "";
  type = id[0];

  for (var i = 0; i < position.length; i++) {
    pose = position[i];
    if (pose.ptype == 'p' && type == 'p') {
      alert('Only one player allowed at the moment!');
      pose.splice(i, 1);
    }
  }
  ctx.drawImage(dropElement, bufferX, bufferY, cellheight, cellwidth);
  if (screen == 1) {
    bufferX2 = bufferX;
  }
  if(screen > 1){
    bufferX2 = bufferX + (screen-1)*WIDTH;
  }

  for (var i = 0; i < position.length; i++) {
    pose = position[i];
    if (pose.px == bufferX2 && pose.py == bufferY) {
      if(pose.ptype != 'b'){
        ctx.clearRect(pose.px, pose.py, cellheight, cellwidth);
        position.splice(i, 1);
        redraw_loaded(position);
        ctx.drawImage(dropElement, bufferX2, bufferY, cellheight, cellwidth);
      }
    }
  }
  position.push({ px: bufferX2, py: bufferY, pimg: new_image, ptype: type, pname: entity_name[entity_name.length - 1] });
}

//'drop' for click instead of drag
function drop2(ev) {
  ev.preventDefault();
  //if in menu or saving - don't drop tile
  if (holdTile){
    return;
  }

  var dropX = ev.clientX - canvasLeft;
  var dropY = ev.clientY - canvasTop;
  let new_image = new Image();
  if (tileToPlace[0] == null){
    return;
  }
  new_image.src = tileToPlace[0].src;
  let entity_name = new_image.src.split('/');
  var id = entity_name[entity_name.length-1];
  var dropElement = new_image;
  // draw the drag image at the drop coordinates
  bufferX = Math.floor(dropX / cellwidth) * cellheight;
  bufferY = Math.floor(dropY / cellwidth) * cellheight;
  let type = "";
  type = id[0];

  for (var i = 0; i < position.length; i++) {
    pose = position[i];
    if (pose.ptype == 'p' && type == 'p') {
      alert('Only one player allowed at the moment!');
      pose.splice(i, 1);
    }
  }
  ctx.drawImage(dropElement, bufferX, bufferY, cellheight, cellwidth);
  if (screen == 1) {
    bufferX2 = bufferX;
  }
  if(screen > 1){
    bufferX2 = bufferX + (screen-1)*WIDTH;
  }

  for (var i = 0; i < position.length; i++) {
    pose = position[i];
    if (pose.px == bufferX2 && pose.py == bufferY) {
      if(pose.ptype != 'b'){
        ctx.clearRect(pose.px, pose.py, cellheight, cellwidth);
        position.splice(i, 1);
        redraw_loaded(position);
        ctx.drawImage(dropElement, bufferX2, bufferY, cellheight, cellwidth);
      }
    }
  }
  position.push({ px: bufferX2, py: bufferY, pimg: new_image, ptype: type, pname: entity_name[entity_name.length - 1] });
}

function saveButtonClicked() {
  document.getElementById('levelEditorSaveMenu').style.display = 'block';
  canvas.removeEventListener('dblclick', doubleClickListener);
  canvas.removeEventListener('single_double_click', single_double_click);
  holdTile = true;
  canvas.addEventListener('click', saveButtonMenuListener);
  radius = 10;
  for (i = 0; i < 3; i++) {
    ctx.beginPath();
    ctx.moveTo(saveMenuButtons[i].x + radius, saveMenuButtons[i].y);
    ctx.lineTo(saveMenuButtons[i].x + saveMenuButtons[i].width - radius, saveMenuButtons[i].y);
    ctx.quadraticCurveTo(saveMenuButtons[i].x + saveMenuButtons[i].width, saveMenuButtons[i].y, saveMenuButtons[i].x + saveMenuButtons[i].width, saveMenuButtons[i].y + radius);
    ctx.lineTo(saveMenuButtons[i].x + saveMenuButtons[i].width, saveMenuButtons[i].y + saveMenuButtons[i].height - radius);
    ctx.quadraticCurveTo(saveMenuButtons[i].x + saveMenuButtons[i].width, saveMenuButtons[i].y + saveMenuButtons[i].height, saveMenuButtons[i].x + saveMenuButtons[i].width - radius, saveMenuButtons[i].y + saveMenuButtons[i].height);
    ctx.lineTo(saveMenuButtons[i].x + radius, saveMenuButtons[i].y + saveMenuButtons[i].height);
    ctx.quadraticCurveTo(saveMenuButtons[i].x, saveMenuButtons[i].y + saveMenuButtons[i].height, saveMenuButtons[i].x, saveMenuButtons[i].y + saveMenuButtons[i].height - radius);
    ctx.lineTo(saveMenuButtons[i].x, saveMenuButtons[i].y + radius);
    ctx.quadraticCurveTo(saveMenuButtons[i].x, saveMenuButtons[i].y, saveMenuButtons[i].x + radius, saveMenuButtons[i].y);
    ctx.fillStyle = '#FFFFFF';
    ctx.fillStyle = 'rgba(124,193,255,1.0)'; // Color of Regular Buttons & Unlocked Levels 
    ctx.fill();
    ctx.lineWidth = 2;
    ctx.strokeStyle = '#757BE8'; //Color Of Box Outline
    ctx.stroke();
    ctx.closePath();
    ctx.fillStyle = '#000000'; //Color of Text
    ctx.textAlign = 'center';
    ctx.font = '30pt Impact';
    ctx.fillText(saveMenuButtons[i].text, saveMenuButtons[i].x + saveMenuButtons[i].width / 2, saveMenuButtons[i].y + (saveMenuButtons[i].height * .7));
  }
}
function saveButtonMenuListener(evt) {
  let mousePos = getMousePos(canvas, evt);
  if ((isInside(mousePos, saveMenuButtons[1]))) {
    ctx.clearRect(0, 0, WIDTH, HEIGHT);
    if (background_image[0] !== undefined) {
      ctx.drawImage(background_image[0].bimg, 0, 0, WIDTH, HEIGHT);
    }
    redraw_loaded(position);
    drawGrid();
    document.getElementById('levelEditorSaveMenu').style.display = 'none';
    canvas.addEventListener('dblclick', doubleClickListener);
    canvas.addEventListener('single_double_click', single_double_click);
    setTimeout(function(){holdTile = false}, 150);
  } else if (isInside(mousePos, saveMenuButtons[2])) {
    save_me();
    setTimeout(function(){holdTile = false}, 150);
  }
}


function save_me() {
  let levelName = levelEditorLevelNameField.value;
  let levelDesc = levelEditorLevelDescField.value;
  socket.emit('saveLevel', { tileArray: position, levelName: levelName, userID: userID, levelDesc: levelDesc, levelID: levelEditorLevelID });
  localStorage.setItem("state", JSON.stringify(position));
  //a = JSON.parse(localStorage.getItem("state") || "[]");
  var retrievedObject = localStorage.getItem("state");
}
socket.on('LevelSaveVerif', function(data){
  if (data.success === true) {
    document.getElementById('levelEditorSaveMenu').style.display = 'none';
    backToMenu();
    levelEditorLevelID = null;
    levelEditorLevelNameField.value = '';
    levelEditorLevelDescField.value = '';
    canvas.removeEventListener('click', saveButtonMenuListener);
  } else {
    alert(data.err);
  }
});

function handleMouseDown(e) {
  canMouseX = parseInt(e.clientX - offsetX);
  canMouseY = parseInt(e.clientY - offsetY);

  for (var i = 0; i < position.length; i++) {
    pose = position[i];
    if (canMouseX > pose.px && canMouseX < pose.px + cellheight && canMouseY > pose.py && canMouseY < pose.py + cellwidth) {
      isDragging = true;
    }
  }
}

function redraw_loaded(buffer_array){
  let name = new Image();
  for (var i = 0; i < buffer_array.length; i++) {
    pose = buffer_array[i];
    if (pose.ptype == 'b') {
      tr = './client/assets/';
      name.src = tr.concat(String(pose.pname));
      ctx.drawImage(name, 0, 0, WIDTH, HEIGHT);
      drawGrid();
    }
  }
  for (var i = 0; i < buffer_array.length; i++) {
    pose = buffer_array[i];
    if (pose.ptype != 'b' && screen == 1 && pose.px < 1280) {
      tr = './client/assets/';
      name.src = tr.concat(String(pose.pname));
      ctx.drawImage(name, pose.px, pose.py, cellwidth, cellheight);
      drawGrid();
    }
    if (pose.ptype != 'b' && screen == 2 && pose.px >= 1280 && pose.px < 2560) {
      tr = './client/assets/';
      name.src = tr.concat(String(pose.pname));
      ctx.drawImage(name, pose.px - 1280, pose.py, cellwidth, cellheight);
      drawGrid();
    }
    if (pose.ptype != 'b' && screen == 3 && pose.px >= 2560 && pose.px < 3840) {
      tr = './client/assets/';
      name.src = tr.concat(String(pose.pname));
      ctx.drawImage(name, pose.px - 2560, pose.py, cellwidth, cellheight);
      drawGrid();
    }
  }
}

function loadLevel(posit) {
  canvas.addEventListener('dblclick', doubleClickListener);
  position = posit;
  buffer_array = posit;
  let name = new Image();
  for (var i = 0; i < posit.length; i++) {
    pose = posit[i];
    if (pose.ptype === 'b') {
      background_image = [];
      tr = './client/assets/';
      name.src = tr.concat(String(pose.pname));
      background_image.push({ bimg:name, bname: pose.pname, type: 'b' });
      ctx.drawImage(name, 0, 0, WIDTH, HEIGHT);
    }
  }
  for (var i = 0; i < posit.length; i++) {
    pose = posit[i];
    if (pose.ptype != 'b' && screen == 1 && pose.px < 1280) {
      tr = './client/assets/';
      name.src = tr.concat(String(pose.pname));
      ctx.drawImage(name, pose.px, pose.py, cellwidth, cellheight);
    }
  }
  drawGrid();
  holdTile = false;
}

//prevents double click from using single click function - so deleting a tile doesn't place a tile
function single_double_click(ev) {
  clicks++;
  if (clicks == 1) {
    var clickWait = setTimeout(function () {
      if (clicks == 1){
        clickToPlaceListener(ev);
      }
      clicks = 0;
    }, 150);
  } else {
    clearTimeout(clickWait);
    clicks = 0;
  }
}

function clickToPlaceListener(ev) {
  drop2(ev);
}

function doubleClickListener() {
  //prevent tile placement
  let name = new Image();
  holdTile = true;
  for (var i = 0; i < position.length; i++) {
    pose = position[i];
    if(screen == 1 && pose.px < 1280){
      if (canMouseX > pose.px && canMouseX < pose.px + cellheight && canMouseY > pose.py && canMouseY < pose.py + cellwidth) {
        ctx.clearRect(pose.px, pose.py, cellheight, cellwidth);
        position.splice(i, 1);
      }
    }
    if(screen == 2 && pose.px >= 1280 && pose.px < 2560){
      if (canMouseX > pose.px-1280 && canMouseX < pose.px-1280 + cellheight && canMouseY > pose.py && canMouseY < pose.py + cellwidth) {
        ctx.clearRect(pose.px-1280, pose.py, cellheight, cellwidth);
        position.splice(i, 1);
      }
    } 
    if(screen == 3 && pose.px >= 2560 && pose.px < 3840){
      if (canMouseX > pose.px-2560 && canMouseX < pose.px-2560 + cellheight && canMouseY > pose.py && canMouseY < pose.py + cellwidth) {
        ctx.clearRect(pose.px-2560, pose.py, cellheight, cellwidth);
        position.splice(i, 1);
      }
    }  
  }
  redraw_loaded(position);
  //allow placing a tile again
  holdTile = false;
}

canvas.addEventListener('dblclick', doubleClickListener);
canvas.addEventListener('single_double_click', single_double_click);
holdTile = false;
